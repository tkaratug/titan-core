<?php

namespace Titan\Controller;

use Titan\Kernel\Application;

trait ControllerTrait
{
    protected Application $app;

    /**
     * Set application container
     */
    protected function setApplication()
    {
        $this->app = \Titan\Kernel\Facade::getFacadeApplication();
    }

    /**
     * Get container service
     *
     * @param string $key
     * @return mixed
     */
    protected function getService(string $key)
    {
        return $this->app->resolve($key);
    }

    /**
     * Returns true if the service is defined
     *
     * @param string $key
     * @return bool
     */
    protected function hasService(string $key) : bool
    {
        return $this->app->isBinded($key);
    }

    /**
     * Generates a URL from the given parameters
     *
     * @param string $route
     * @param array $params
     * @return mixed
     */
    protected function getUrl(string $route, array $params = [])
    {
        return $this->app->resolve('router')->getUrl($route, $params);
    }

    /**
     * Generates an URL with language prefix from the given parameters
     *
     * @param string $route
     * @param array $params
     * @return mixed
     */
    protected function getUrlWithLang(string $route, array $params = [])
    {
        $language = $this->app->resolve('language')->get()['code'];
        return $this->app->resolve('router')->getUrl($route, $params, $language);
    }

    /**
     * Generates a path from the given parameters
     *
     * @param string $route
     * @param array $params
     * @return mixed
     */
    protected function getPath(string $route, array $params = [])
    {
        return $this->app->resolve('router')->getPath($route, $params);
    }

    /**
     * Generates an URL with language prefix from the given parameters
     *
     * @param string $route
     * @param array $params
     * @return mixed
     */
    protected function getPathWithLang(string $route, array $params = [])
    {
        $language = $this->app->resolve('language')->get()['code'];
        return $this->app->resolve('router')->getPath($route, $params, $language);
    }

    /**
     * Redirect to the given url
     *
     * @param string $url
     * @param int $delay
     * @return mixed
     */
    protected function redirect(string $url, int $delay = 0)
    {
        return $this->app->resolve('uri')->redirect($url, $delay);
    }

    /**
     * Redirects to the route with the given parameters
     *
     * @param string $route
     * @param array $params
     * @return mixed
     */
    protected function redirectToRoute(string $route, array $params = [])
    {
        return $this->app->resolve('uri')->redirect($this->getUrl($route, $params));
    }

    /**
     * Returns a json response
     *
     * @param $data
     * @param int $status
     * @return mixed
     */
    protected function json($data, int $status = 200)
    {
        return $this->app->resolve('response')->setStatusCode($status)->json($data);
    }

    /**
     * Set a flash message to the current session
     *
     * @param mixed $message
     * @param string|null $url
     * @return mixed
     */
    protected function setFlash($message, string $url = null)
    {
        return $this->app->resolve('session')->setFlash($message, $url);
    }

    /**
     * Get flash message from current session
     *
     * @return mixed
     */
    protected function getFlash()
    {
        return $this->app->resolve('session')->getFlash();
    }

    /**
     * Returns a rendered view
     *
     * @param string $view
     * @param array $params
     * @param bool $cache
     * @return mixed
     */
    protected function render(string $view, array $params = [], bool $cache = true)
    {
        return $this->app->resolve('view')->render($view, $params, $cache);
    }

    /**
     * Run middlewares from the given parameters
     *
     * @param array $middleware
     * @throws \ReflectionException
     */
    protected function middleware(array $middleware)
    {
        foreach ($middleware as $class) {
            $class  = $this->app->resolve($class);
            $action = new \ReflectionMethod($class, 'handle');
            $params = $action->getParameters();
            $args   = [];

            foreach ($params as $param) {
                if (isset($param->getClass()->name)) {
                    $paramClass = $param->getClass()->name;
                    $args[] = $this->app->resolve($paramClass);
                }
            }

            $action->invokeArgs($class, $args);
        }
    }

    /**
     * Returns a DB instance
     *
     * @return mixed
     */
    protected function db()
    {
        return $this->app->resolve('db');
    }
}
