<?php

namespace Titan\Controller;

class Controller
{
    use ControllerTrait;

    /**
     * Controller constructor.
     *
     * @throws \ReflectionException
     */
    public function __construct()
    {
        $this->setApplication();
        $this->middleware($this->app->resolve('config')->get('services.middleware')['default']);
    }
}