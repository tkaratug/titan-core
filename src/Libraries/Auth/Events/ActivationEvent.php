<?php

namespace Titan\Libraries\Auth\Events;

class ActivationEvent
{
    /**
     * @var object
     */
    public $user;

    /**
     * @var string
     */
    public $code;

    /**
     * @var array
     */
    public $config;

    /**
     * Event constructor.
     */
    public function __construct($user, string $code, array $config)
    {
        $this->user     = $user;
        $this->code     = $code;
        $this->config   = $config;
    }
}