<?php

namespace Titan\Libraries\Auth\Events;

class ResetPasswordEvent
{
    /**
     * @var object
     */
    public $user;

    /**
     * @var string
     */
    public $token;

    /**
     * @var array
     */
    public $config;

    /**
     * Event constructor.
     */
    public function __construct($user, string $token, array $config)
    {
        $this->user     = $user;
        $this->token    = $token;
        $this->config   = $config;
    }
}