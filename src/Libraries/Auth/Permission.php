<?php

namespace Titan\Libraries\Auth;

use Titan\Facades\Cache;
use Titan\Facades\Model;

class Permission
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $display_name;

    /**
     * @var string
     */
    public $description;

    /**
     * Permission model
     *
     * @var \App\Models\Auth\Permission
     */
    private $permissionModel;

    /**
     * Permission constructor.
     */
    public function __construct()
    {
        $this->permissionModel = Model::use(config('auth.models.permission'));
    }

    /**
     * Save permission data
     *
     * @return mixed
     */
    public function save()
    {
        $save = $this->permissionModel->save($this);

        if ($save) {
            $this->clearCache();
        }

        return $save;
    }

    /**
     * Delete permission
     *
     * @return mixed
     */
    public function delete()
    {
        $delete = $this->permissionModel->delete($this);

        if ($delete) {
            $this->clearCache();
        }

        return $delete;
    }

    /**
     * Clear permission cache
     */
    private function clearCache()
    {
        Cache::setFileName('role-permissions')->clear();
        Cache::setFileName('user-permissions')->clear();
    }
}