<?php

namespace Titan\Libraries\Auth\Factory;

use Titan\Exception\NotFoundHttpException;
use Titan\Facades\Model;
use Titan\Libraries\Auth\Permission;

class PermissionFactory
{
    /**
     * Create permission
     *
     * @param object $data
     * @return Permission
     */
    public static function createPermission($data)
    {
        return self::create($data);
    }

    /**
     * Create permission by name
     *
     * @param string $name
     * @return Permission
     */
    public static function createPermissionByName(string $name)
    {
        $data = Model::use(\App\Models\Auth\Permission::class)->getByName($name);

        if (!$data) {
            throw new NotFoundHttpException('Couldn\'t find a permission as ' . $name);
        }

        return self::create($data);
    }

    /**
     * Create new empty permission
     *
     * @return Permission
     */
    public static function createNewPermission()
    {
        return new Permission();
    }

    /**
     * Create permission by given data
     *
     * @param $data
     * @return Permission
     */
    private static function create($data)
    {
        $permission = new Permission();

        $permission->id = $data->id;
        $permission->name = $data->name;
        $permission->display_name = $data->display_name;
        $permission->description = $data->description;

        return $permission;
    }
}