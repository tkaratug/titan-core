<?php

namespace Titan\Libraries\Auth\Factory;

use Titan\Exception\NotFoundHttpException;
use Titan\Facades\Cache;
use Titan\Facades\Model;
use Titan\Libraries\Auth\Role;

class RoleFactory
{
    /**
     * Create role
     *
     * @param object $data
     * @return Role
     */
    public static function createRole($data)
    {
        return self::create($data);
    }

    /**
     * Create role by name
     * 
     * @param string $name
     * @return Role
     */
    public static function createRoleByName(string $name)
    {
        $data = Model::use(\App\Models\Auth\Role::class)->getByName($name);

        if (!$data) {
            throw new NotFoundHttpException('Couldn\'t find a role as ' . $name);
        }

        return self::create($data);
    }

    /**
     * Create new empty role
     *
     * @return Role
     */
    public static function createNewRole()
    {
        return new Role();
    }

    /**
     * Create role by given data
     *
     * @param object $data
     * @return Role
     */
    private static function create($data)
    {
        $role = new Role();

        $role->id = $data->id;
        $role->name = $data->name;
        $role->display_name = $data->display_name;
        $role->description = $data->description;
        $role->permissions = self::getPermissions($data->id);

        return $role;
    }

    /**
     * Get role permissions
     *
     * @param int $roleId
     * @return mixed
     */
    private static function getPermissions(int $roleId)
    {
        Cache::setFileName('role-permissions');

        if (!Cache::has('permissions_' . $roleId)) {
            $permissions = Model::use(\App\Models\Auth\Role::class)->getPermissions($roleId);
            $arr = [];

            foreach ($permissions as $permission) {
                $arr[] = PermissionFactory::createPermission($permission);
            }

            Cache::save('permissions_' . $roleId, $arr);
        }

        return Cache::read('permissions_' . $roleId);
    }
}