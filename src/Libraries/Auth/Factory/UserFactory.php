<?php

namespace Titan\Libraries\Auth\Factory;

use Titan\Facades\Cache;
use Titan\Libraries\Auth\User;
use Titan\Facades\Model;

class UserFactory
{
    /**
     * Create user
     *
     * @param int|null $id
     * @return User
     */
    public static function createUser(int $id = null)
    {
        $user = new User();

        if (!empty($id)) {
            $user->props        = Model::use(\App\Models\Auth\User::class)->getUserById($id);
            $user->roles        = self::getRoles($id);
            $user->permissions  = self::getPermissions($id);
        }

        return $user;
    }

    /**
     * Create user from session
     *
     * @param object $sessionUser
     * @return User
     */
    public static function createUserFromSession($sessionUser)
    {
        $user = new User();
        $user->props        = $sessionUser;
        $user->roles        = self::getRoles($sessionUser->id);
        $user->permissions  = self::getPermissions($sessionUser->id);

        return $user;
    }

    /**
     * Get user roles
     *
     * @param int $userId
     * @return mixed
     */
    private static function getRoles(int $userId)
    {
        Cache::setFileName('user-roles');

        if (!Cache::has('roles_' . $userId)) {
            $roles  = Model::use(\App\Models\Auth\User::class)->getRoles($userId);
            $arr    = [];

            foreach ($roles as $role) {
                $arr[] = RoleFactory::createRole($role);
            }

            Cache::save('roles_' . $userId, $arr);
        }

        return Cache::read('roles_' . $userId);
    }

    /**
     * Get user permissions
     *
     * @param int $userId
     * @return mixed
     */
    private static function getPermissions(int $userId)
    {
        Cache::setFileName('user-permissions');

        if (!Cache::has('permissions_' . $userId)) {
            $permissions = Model::use(\App\Models\Auth\User::class)->getPermissions($userId);
            $arr = [];

            foreach ($permissions as $permission) {
                $arr[] = PermissionFactory::createPermission($permission);
            }

            Cache::save('permissions_' . $userId, $arr);
        }

        return Cache::read('permissions_' . $userId);
    }
}