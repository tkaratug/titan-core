<?php

namespace Titan\Libraries\Auth;

use Titan\Exception\BadRequestHttpException;
use Titan\Exception\NotFoundHttpException;
use Titan\Facades\Cache;
use Titan\Facades\Model;

class User
{
    /**
     * User properties
     *
     * @var object
     */
    public $props;

    /**
     * User roles
     *
     * @var array
     */
    public $roles = [];

    /**
     * User permissions
     *
     * @var array
     */
    public $permissions = [];

    /**
     * User model
     *
     * @var \App\Models\Auth\User
     */
    private $userModel;

    /**
     * Role model
     *
     * @var \App\Models\Auth\Role
     */
    private $roleModel;

    /**
     * Permission model
     *
     * @var \App\Models\Auth\Permission
     */
    private $permissionModel;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->userModel = Model::use(config('auth.models.user'));
        $this->roleModel = Model::use(config('auth.models.role'));
        $this->permissionModel = Model::use(config('auth.models.permission'));
    }

    /**
     * Get user roles
     *
     * @return array
     */
    public function roles()
    {
        return $this->roles;
    }

    /**
     * Get user permissions
     *
     * @return array
     */
    public function permissions(): array
    {
        return $this->permissions;
    }

    /**
     * Attach roles to the user
     *
     * @param array|string $roles
     * @return bool
     */
    public function attachRole($roles): bool
    {
        if (is_array($roles)) {
            foreach ($roles as $roleName) {
                if (!$this->hasRole($roleName)) {
                    $role = $this->roleModel->getByName($roleName);

                    if (!$role) {
                        throw new NotFoundHttpException('Couldn\'t find a role as ' . $roleName);
                    }

                    $this->userModel->attachRole($this->props->id, $role->id);
                    $this->roles[] = $role;
                }
            }
        } else {
            if (!$this->hasRole($roles)) {
                $role = $this->roleModel->getByName($roles);

                if (!$role) {
                    throw new NotFoundHttpException('Couldn\'t find a role as ' . $roles);
                }

                $this->userModel->attachRole($this->props->id, $role->id);
                $this->roles[] = $role;
            }
        }

        $this->clearCache('user-roles', 'roles_' . $this->props->id);

        return true;
    }

    /**
     * Detach role from the user
     *
     * @param array|string $roles
     * @return bool
     */
    public function detachRole($roles): bool
    {
        if (is_array($roles)) {
            foreach ($roles as $roleName) {
                if ($this->hasRole($roleName)) {
                    $role = $this->roleModel->getByName($roleName);

                    if (!$role) {
                        throw new NotFoundHttpException('Couldn\'t find a role as ' . $roleName);
                    }

                    $this->userModel->detachRole($this->props->id, $role->id);
                    unset($this->roles[$this->flipRoles()[$role->id]]);
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                $role = $this->roleModel->getByName($roles);

                if (!$role) {
                    throw new NotFoundHttpException('Couldn\'t find a role as ' . $roles);
                }

                $this->userModel->detachRole($this->props->id, $role->id);
                unset($this->roles[$this->flipRoles()[$role->id]]);
            }
        }

        $this->clearCache('user-roles', 'roles_' . $this->props->id);

        return true;
    }

    /**
     * Check whether the user has the role
     *
     * @param string $role
     * @return bool
     */
    public function hasRole(string $role): bool
    {
        $roles = array_flip(array_column($this->roles, 'name'));

        return isset($roles[$role]);
    }

    /**
     * Attach permission to the user
     *
     * @param array|string $permissions
     * @return bool
     */
    public function attachPermission($permissions): bool
    {
        if (is_array($permissions)) {
            foreach ($permissions as $permissionName) {
                if (!$this->hasPermission($permissionName)) {
                    $permission = $this->permissionModel->getByName($permissionName);

                    if (!$permission) {
                        throw new NotFoundHttpException('Couldn\'t find a permission as ' . $permissionName);
                    }

                    $this->userModel->attachPermission($this->props->id, $permission->id);
                    $this->permissions[] = $permission;
                }
            }
        } else {
            if (!$this->hasPermission($permissions)) {
                $permission = $this->permissionModel->getByName($permissions);

                if (!$permission) {
                    throw new NotFoundHttpException('Couldn\'t find a permission as ' . $permissions);
                }

                $this->userModel->attachPermission($this->props->id, $permission->id);
                $this->permissions[] = $permission;
            }
        }

        $this->clearCache('user-permissions', 'permissions_' . $this->props->id);

        return true;
    }

    /**
     * Detach permission from the user
     *
     * @param array|string $permissions
     * @return bool
     */
    public function detachPermission($permissions): bool
    {
        if (is_array($permissions)) {
            foreach ($permissions as $permissionName) {
                if ($this->hasPermission($permissionName)) {
                    $permission = $this->permissionModel->getByName($permissionName);

                    if (!$permission) {
                        throw new NotFoundHttpException('Couldn\'t find a permission as ' . $permissionName);
                    }

                    $this->userModel->detachPermission($this->props->id, $permission->id);
                    unset($this->permissions[$this->flipPermissions()[$permission->id]]);
                }
            }
        } else {
            if ($this->hasPermission($permissions)) {
                $permission = $this->permissionModel->getByName($permissions);

                if (!$permission) {
                    throw new NotFoundHttpException('Couldn\'t find a permission as ' . $permissions);
                }

                $this->userModel->detachPermission($this->props->id, $permission->id);
                unset($this->permissions[$this->flipPermissions()[$permission->id]]);
            }
        }

        $this->clearCache('user-permissions', 'permissions_' . $this->props->id);

        return true;
    }

    /**
     * Check whether the user has the permission
     *
     * @param string $permission
     * @return bool
     */
    public function hasPermission(string $permission): bool
    {
        $permissions = array_flip(array_column($this->permissions, 'name'));

        return isset($permissions[$permission]);
    }

    /**
     * Check whether the user can do given action
     *
     * @param string $permission
     * @return bool
     */
    public function can(string $permission): bool
    {
        // Check user permission first
        if ($this->hasPermission($permission)) {
            return true;
        }

        // If user does not have the permission directly, check roles
        foreach ($this->roles() as $role) {
            if ($role->hasPermission($permission)) {
                return true;
            }
        }
    }

    /**
     * Flip roles array
     *
     * @return array
     */
    private function flipRoles(): array
    {
        return array_flip(array_column($this->roles, 'id'));
    }

    /**
     * Flip permissions array
     *
     * @return array
     */
    private function flipPermissions(): array
    {
        return array_flip(array_column($this->permissions, 'id'));
    }

    /**
     * Delete cached roles
     */
    private function clearCache(string $file, string $key)
    {
        Cache::setFileName($file);

        if (Cache::has($key)) {
            Cache::delete($key);
        }
    }

    /**
     * Check user props
     */
    private function checkProps()
    {
        if (!is_object($this->props)) {
            throw new BadRequestHttpException('User object has no property!');
        }
    }

    /**
     * @param $method
     * @param $arguments
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        if (method_exists($this, $method)) {
            $this->checkProps();
            return call_user_func_array([$this, $method], $arguments);
        }
    }
}