<?php

namespace Titan\Libraries\Auth;

use Titan\Kernel\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Auth::class)->alias('auth');
    }
}