<?php

namespace Titan\Libraries\Auth\Listeners;

use Titan\Exception\ExceptionHandler;
use Titan\Libraries\Auth\Events\ActivationEvent;
use Titan\Facades\Mail;

class ActivationListener
{
    /**
     * Handle
     *
     * @param ActivationEvent $event
     * @return mixed
     * @throws ExceptionHandler
     */
    public function handle(ActivationEvent $event)
    {
        if (!empty(config('mail.server'))) {
            return Mail::from($event->config['notification']['from_address'])
                ->to([$event->user->email])
                ->subject(translate('auth', 'activation_mail_title'))
                ->body(view()->content('auth.email.activation_email', ['user' => $event->user, 'code' => $event->code]))
                ->send();
        }

        throw new ExceptionHandler('Config Error', 'Mail parameters are not set!');
    }
}