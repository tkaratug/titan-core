<?php

namespace Titan\Libraries\Auth\Listeners;

use Titan\Exception\ExceptionHandler;
use Titan\Libraries\Auth\Events\ResetPasswordEvent;
use Titan\Facades\Mail;

class ResetPasswordListener
{
    /**
     * Handle
     *
     * @param ResetPasswordEvent $event
     * @return mixed
     * @throws ExceptionHandler
     */
    public function handle(ResetPasswordEvent $event)
    {
        if (!empty(config('mail.server'))) {
            return Mail::from($event->config['notification']['from_address'])
                ->to([$event->user->email])
                ->subject(translate('auth', 'reset_mail_title'))
                ->body(view()->content('auth.email.reset_password_email', ['user' => $event->user, 'token' => $event->token]))
                ->send();
        }

        throw new ExceptionHandler('Config Error', 'Mail parameters are not set!');
    }
}