<?php

namespace Titan\Libraries\Auth;

use Titan\Kernel\Application;
use Titan\Libraries\Auth\Events\ActivationEvent;
use Titan\Libraries\Auth\Events\ResetPasswordEvent;
use Titan\Facades\{Hash, Model, Session, Event, Date, Request};
use Titan\Libraries\Auth\Factory\PermissionFactory;
use Titan\Libraries\Auth\Factory\RoleFactory;
use Titan\Libraries\Auth\Factory\UserFactory;

class Auth
{
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Model
     */
    protected $model;

    /**
     * Auth constructor.
     */
    public function __construct(Application $app)
    {
        $this->app      = $app;
        $this->config   = $app->resolve('config')->get('auth');
        $this->model    = Model::use($this->config['models']['user']);
    }

    /**
     * Get config parameters for authentication
     *
     * @param string $param
     * @return mixed
     */
    public function config(string $param)
    {
        return $this->config[$param];
    }

    /**
     * Is duplicate email
     *
     * @param string $email
     * @return bool
     */
    public function isDuplicateEmail(string $email): bool
    {
        if (!empty($this->model->getUserByMail($email))) {
            return true;
        }

        return false;
    }

    /**
     * Register
     *
     * @param array $request
     * @return bool
     * @throws \Exception
     */
    public function register(array $request)
    {
        // Remove password_confirm key from the request array
        unset($request['password_confirm']);

        // Remove csrf_token from the request array
        unset($request['csrf_token']);

        // Hash the password
        $request['password'] = $this->app->resolve('hash')->make($request['password']);

        // if activation is set as true
        if ($this->config['activation'] === true) {
            $request['activation_code'] = $this->generateToken(16);
            $request['active']          = 0;
        }

        // If register successful set the session data
        if ($user = $this->model->register($request)) {

            // If activation is set as true, send activation mail
            if ($this->config['activation'] === true) {
                Event::stack('core')->fire(new ActivationEvent($user, $request['activation_code'], $this->config));
            }

            // return true
            return true;
        }

        // Throw exception
        throw new \Exception(translate('auth', 'register_fail'));
    }

    /**
     * Login
     *
     * @param string $email
     * @param string $password
     * @return bool
     * @throws \Exception
     */
    public function login(string $email, string $password)
    {
        // Check failed attempts at first
        $this->checkAttempts($email, Request::getIp());

        // Find user
        $user = $this->model->getUserByMail($email);

        // If user not found throw an exception
        if (empty($user)) {
            $this->addAttempt($email, Request::getIp());
            throw new \Exception(translate('auth', 'user_not_found'));
        }

        // If password is wrong throw an exception
        if (!password_verify($password, $user->password)) {
            $this->addAttempt($email, Request::getIp());
            throw new \Exception(translate('auth', 'wrong_password'));
        }

        // If user is inactive throw an exception
        if (!$user->active) {
            $this->addAttempt($email, Request::getIp());
            throw new \Exception(translate('auth', 'inactive_user', ['%s' => route('activationForm')]));
        }

        // Unser user password
        unset($user->password);

        // Set session data
        $this->app->resolve('session')->set('user', $user);

        return true;
    }

    /**
     * Logout
     *
     * @return mixed
     */
    public function logout()
    {
        return $this->app->resolve('session')->delete('user');
    }

    /**
     * Reset password
     *
     * @param string $email
     * @throws \Exception
     */
    public function resetPassword(string $email)
    {
        // Find user
        $user = $this->model->getUserByMail($email);

        // If user not found throw an error
        if (empty($user)) {
            throw new \Exception(translate('auth', 'user_not_found'));
        }

        // Generate token
        $token = $this->generateToken();

        // Save token to database
        $this->model->saveResetPasswordToken($email, $token);

        // Send mail
        Event::stack('core')->fire(new ResetPasswordEvent($user, $token, $this->app->resolve('config')->get('auth')));
    }

    /**
     * Get reset password data
     *
     * @param string $token
     * @return mixed
     */
    public function getResetPasswordToken(string $token)
    {
        return $this->model->getResetPasswordToken($token);
    }

    /**
     * Set new password
     *
     * @param string $password
     * @param string $email
     * @return mixed
     */
    public function setNewPassword(string $password, string $email)
    {
        return $this->model->setNewPassword(Hash::make($password), $email);
    }

    /**
     * Activate user
     *
     * @param string $code
     * @return mixed
     * @throws \Exception
     */
    public function activate(string $code)
    {
        // Find the user with given activation code
        $user = $this->model->getUserByActivationCode($code);

        // If user not found, throw an exception
        if (empty($user)) {
            throw new \Exception(translate('auth', 'activation_invalid'));
        }

        // Activate user and return
        return $this->model->activateUser($user->id);
    }

    /**
     * Resend the activation code
     *
     * @param string $email
     * @return bool
     * @throws \Exception
     */
    public function resendActivation(string $email)
    {
        // Find the user with given email address
        $user = $this->model->getUserByMail($email);

        // If user not found, throw an exception
        if (empty($user)) {
            throw new \Exception(translate('auth', 'user_not_found'));
        }

        // If the user is active, throw an exception
        if ($user->active) {
            throw new \Exception(translate('auth', 'user_already_active'));
        }

        // Send mail
        Event::stack('core')->fire(new ActivationEvent($user, $user->activation_code, $this->config));

        // return
        return true;
    }

    /**
     * Get user data in session
     *
     * @return mixed
     */
    public function user()
    {
        if (Session::has('user')) {
            return UserFactory::createUserFromSession(Session::get('user'));
        }

        return UserFactory::createUser();
    }

    /**
     * Get role by given name or create empty
     *
     * @param string $role
     * @return Role
     */
    public function role(string $role = null)
    {
        if (is_null($role)) {
            return RoleFactory::createNewRole();
        }

        return RoleFactory::createRoleByName($role);
    }

    /**
     * Get permission by given name or create empty
     *
     * @param string|null $permission
     * @return Permission
     */
    public function permission(string $permission = null)
    {
        if (is_null($permission)) {
            return PermissionFactory::createNewPermission();
        }

        return PermissionFactory::createPermissionByName($permission);
    }

    /**
     * Add failed attempt
     *
     * @param string $email
     * @param string $ip
     * @return bool
     * @throws \Exception
     */
    private function addAttempt(string $email, string $ip)
    {
        if ($this->config['save_attempts'] === true) {
            // Purge first
            $this->purgeAttempts();

            // Add failed attempt
            $this->model->addFailedAttempt([
                'email'         => $email,
                'ip'            => $ip,
                'created_at'    => date('Y-m-d H:i:s')
            ]);

            // Check failed attempts
            return $this->checkAttempts($email, $ip);
        }

        return true;
    }

    /**
     * Purge failed attempt older than x minutes
     *
     * @return mixed
     */
    private function purgeAttempts()
    {
        $from = Date::now()->subtractMinute($this->config['purge_period'])->get();

        return $this->model->purgeFailedAttempts($from);
    }

    /**
     * Check failed attempts
     *
     * @param string $email
     * @param string $ip
     * @return bool
     * @throws \Exception
     */
    private function checkAttempts(string $email, string $ip)
    {
        // If save_attempts config is true
        if ($this->config['save_attempts'] === true) {

            // Find the exact time that flood time ago
            $from   = Date::now()->subtractMinute($this->config['flood_time'])->get();

            // Get flood time
            $flood  = $this->config['flood_time'];

            if ($this->model->getTotalFailsByMailFrom($email, $from) >= $this->config['fails_email']) {
                $first  = $this->model->getFirstAttemptByMailFrom($email, $from);
                $left   = ($flood * 60) - Date::now()->compare($first->created_at)->getComparisonInSeconds();
                if ($left < 60) {
                    throw new \Exception(translate('auth', 'throttle_email_seconds', ['%s' => $left]));
                }
                throw new \Exception(translate('auth', 'throttle_email_minutes', ['%s' => ceil($left/60)]));
            } else if ($this->model->getTotalFailsByIpFrom($ip, $from) >= $this->config['fails_ip']) {
                $first  = $this->model->getFirstAttemptByIpFrom($ip, $from);
                $left   = ($flood * 60) - Date::now()->compare($first->created_at)->getComparisonInSeconds();
                if ($left < 60) {
                    throw new \Exception(translate('auth', 'throttle_ip_seconds', ['%s' => $left]));
                }
                throw new \Exception(translate('auth', 'throttle_ip_minutes', ['%s' => ceil($left/60)]));
            }
        }

        return true;
    }

    /**
     * Generate token
     *
     * @param int $length
     * @return string
     */
    private function generateToken(int $length = 32): string
    {
        $characters = '0123456789';
        $characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters)-1;
        $token = '';

        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[mt_rand(0, $charactersLength)];
        }

        return $token;
    }
}