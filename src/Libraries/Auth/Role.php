<?php

namespace Titan\Libraries\Auth;

use Titan\Exception\NotFoundHttpException;
use Titan\Facades\Cache;
use Titan\Facades\Model;

class Role
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $display_name;

    /**
     * @var string
     */
    public $description;

    /**
     * @var array
     */
    public $permissions = [];

    /**
     * Role model
     *
     * @var \App\Models\Auth\Role
     */
    private $roleModel;

    /**
     * Permission model
     *
     * @var \App\Models\Auth\Permission
     */
    private $permissionModel;

    /**
     * Role constructor.
     */
    public function __construct()
    {
        $this->roleModel = Model::use(config('auth.models.role'));
        $this->permissionModel = Model::use(config('auth.models.permission'));
    }

    /**
     * Get role permissions
     *
     * @return array
     */
    public function permissions(): array
    {
        return $this->permissions;
    }

    /**
     * Attach permission to the role
     *
     * @param array|string $permissions
     * @return bool
     */
    public function attachPermission($permissions): bool
    {
        if (is_array($permissions)) {
            foreach ($permissions as $permissionName) {
                if (!$this->hasPermission($permissionName)) {
                    $permission = $this->permissionModel->getByName($permissionName);

                    if (!$permission) {
                        throw new NotFoundHttpException('Couldn\'t find a permission as ' . $permissionName);
                    }

                    $this->roleModel->attachPermission($this->id, $permission->id);
                    $this->permissions[] = $permission;
                }
            }
        } else {
            if (!$this->hasPermission($permissions)) {
                $permission = $this->permissionModel->getByName($permissions);

                if (!$permission) {
                    throw new NotFoundHttpException('Couldn\'t find a permission as ' . $permissions);
                }

                $this->roleModel->attachPermission($this->id, $permission->id);
                $this->permissions[] = $permission;
            }
        }

        $this->clearCache('role-permissions', 'permissions_' . $this->id);

        return true;
    }

    /**
     * Detach permission from the role
     *
     * @param array|string $permissions
     * @return bool
     */
    public function detachPermission($permissions): bool
    {
        if (is_array($permissions)) {
            foreach ($permissions as $permissionName) {
                if ($this->hasPermission($permissionName)) {
                    $permission = $this->permissionModel->getByName($permissionName);

                    if (!$permission) {
                        throw new NotFoundHttpException('Couldn\'t find a permission as ' . $permissionName);
                    }

                    $this->roleModel->detachPermission($this->id, $permission->id);
                    unset($this->permissions[$this->flipPermissions()[$permission->id]]);
                }
            }
        } else {
            if ($this->hasPermission($permissions)) {
                $permission = $this->permissionModel->getByName($permissions);

                if (!$permission) {
                    throw new NotFoundHttpException('Couldn\'t find a permission as ' . $permissions);
                }

                $this->roleModel->detachPermission($this->id, $permission->id);
                unset($this->permissions[$this->flipPermissions()[$permission->id]]);
            }
        }

        $this->clearCache('role-permissions', 'permissions_' . $this->id);

        return true;
    }

    /**
     * Check whether the role has the permission
     *
     * @param string $permission
     * @return bool
     */
    public function hasPermission(string $permission): bool
    {
        $permissions = array_flip(array_column($this->permissions, 'name'));

        return isset($permissions[$permission]);
    }

    /**
     * Check whether the role can do given action
     *
     * @param string $permission
     * @return bool
     */
    public function can(string $permission): bool
    {
        return $this->hasPermission($permission);
    }

    /**
     * Save role data
     *
     * @return mixed
     */
    public function save()
    {
        $save = $this->roleModel->save($this);

        if ($save) {
            $this->clearRoleCache();
        }

        return $save;
    }

    /**
     * Delete role
     *
     * @return mixed
     */
    public function delete()
    {
        $delete = $this->roleModel->delete($this);

        if ($delete) {
            $this->clearRoleCache();
        }

        return $delete;
    }

    /**
     * Clear role cache
     */
    private function clearRoleCache()
    {
        Cache::setFileName('role-permissions')->clear();
        Cache::setFileName('user-roles')->clear();
    }

    /**
     * Flip permissions array
     *
     * @return array
     */
    private function flipPermissions()
    {
        return array_flip(array_column($this->permissions, 'id'));
    }

    /**
     * Delete cached roles
     */
    private function clearCache(string $file, string $key)
    {
        Cache::setFileName($file);

        if (Cache::has($key)) {
            Cache::delete($key);
        }
    }
}