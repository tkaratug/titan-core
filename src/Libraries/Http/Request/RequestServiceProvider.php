<?php

namespace Titan\Libraries\Http\Request;

use Titan\Kernel\ServiceProvider;

class RequestServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Request::class)->alias('request');
    }
}