<?php

namespace Titan\Libraries\Http\Client;

use Titan\Kernel\ServiceProvider;

class ClientServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Client::class)->alias('http-client');
    }
}