<?php

namespace Titan\Libraries\Http\Client;

use Titan\Exception\ExceptionHandler;

class Client
{
    /** @var null */
    private $ch = null;

    /** @var bool */
    protected $followRedirects = true;

    /** @var array */
    protected $options = [];

    /** @var array */
    protected $headers = [];

    /** @var string|null */
    protected $referrer = null;

    /** @var string */
    protected $agent = '';

    /** @var string */
    protected $responseBody = '';

    /** @var array */
    protected $responseHeaders = [];

    /** @var string */
    protected $error = '';

    /**
     * Head request
     *
     * @param string $url
     * @param array $params
     * @return $this
     */
    public function head(string $url, array $params = [])
    {
        return $this->request('HEAD', $url, $params);
    }

    /**
     * Get request
     *
     * @param string $url
     * @param array $params
     * @return $this
     */
    public function get(string $url, array $params = [])
    {
        return $this->request('GET', $url, $params);
    }

    /**
     * Post request
     *
     * @param string $url
     * @param array $params
     * @return $this
     */
    public function post(string $url, array $params = [])
    {
        return $this->request('POST', $url, $params);
    }

    /**
     * Put request
     *
     * @param string $url
     * @param array $params
     * @return $this
     */
    public function put(string $url, array $params = [])
    {
        return $this->request('PUT', $url, $params);
    }

    /**
     * Patch request
     *
     * @param string $url
     * @param array $params
     * @return $this
     */
    public function patch(string $url, array $params = [])
    {
        return $this->request('PATCH', $url, $params);
    }

    /**
     * Delete request
     *
     * @param string $url
     * @param array $params
     * @return $this
     */
    public function delete(string $url, array $params = [])
    {
        return $this->request('DELETE', $url, $params);
    }

    /**
     * Define Referrer
     *
     * @param string $referrer
     * @return string
     */
    public function setReferrer(string $referrer)
    {
        return $this->referrer = $referrer;
    }

    /**
     * Define User Agent
     *
     * @param string $agent
     * @return string
     */
    public function setUserAgent(string $agent)
    {
        return $this->agent = $agent;
    }

    /**
     * Define Request Headers
     *
     * @param array $headers
     * @return $this
     */
    public function withHeaders(array $headers = [])
    {
        array_merge($this->headers, $headers);

        return $this;
    }

    /**
     * Define username and password for basic authentication
     *
     * @param string $username
     * @param string $password
     * @return $this
     */
    public function withBasicAuth(string $username, string $password)
    {
        $this->options['CURLOPT_USERPWD'] = $username . ':' . $password;

        return $this;
    }

    /**
     * Define token for api authentication
     *
     * @param string $token
     * @return $this
     */
    public function withToken(string $token)
    {
        $this->headers['Content-Type'] = 'application/json';
        $this->headers['Authorization'] = 'Bearer ' . $token;

        return $this;
    }

    /**
     * Send data using 'application/x-www-form-urlencoded'
     * content type
     *
     * @return $this
     */
    public function asForm()
    {
        $this->headers['Content-Type'] = 'application/x-www-form-urlencoded';

        return $this;
    }

    /**
     * Return headers from the response
     *
     * @return array
     */
    public function headers(): array
    {
        return $this->responseHeaders;
    }

    /**
     * Return a header from the response
     *
     * @param string $key
     * @return string
     * @throws ExceptionHandler
     */
    public function header(string $key): string
    {
        if (!isset($this->responseHeaders[$key])) {
            throw new ExceptionHandler('Parameter not found!', 'The header parameter tht you are looking for does not exist.');
        }

        return $this->responseHeaders[$key];
    }

    /**
     * Return the body of the response
     *
     * @return string
     */
    public function body()
    {
        return $this->responseBody;
    }

    /**
     * Return the status code of the response
     *
     * @return int
     */
    public function status(): int
    {
        return $this->responseHeaders['Status-Code'];
    }

    /**
     * Determine if the request was successful.
     *
     * @return bool
     */
    public function successful(): bool
    {
        return $this->status() >= 200 && $this->status() < 300;
    }

    /**
     * Determine if the response code was "OK".
     *
     * @return bool
     */
    public function ok(): bool
    {
        return $this->status() === 200;
    }

    /**
     * Determine if the response was a redirect.
     *
     * @return bool
     */
    public function redirect(): bool
    {
        return $this->status() >= 300 && $this->status() < 400;
    }

    /**
     * Detemine if the response indicates a client error occurred.
     *
     * @return bool
     */
    public function clientError(): bool
    {
        return $this->status() >= 400 && $this->status() < 500;
    }

    /**
     * Detemine if the response indicates a server error occurred.
     *
     * @return bool
     */
    public function serverError(): bool
    {
        return $this->status() >= 500;
    }

    /**
     * Return error
     *
     * @return string
     */
    public function error()
    {
        return $this->error;
    }

    /**
     * Make Request
     *
     * @param string $method
     * @param string $url
     * @param array $params
     * @return $this
     */
    private function request($method, $url, $params = [])
    {
        $this->error 	= '';
        $this->ch 		= curl_init();

        if (is_array($params)) {
            $params = http_build_query($params, '', '&');
        }

        $this->setRequestMethod($method);
        $this->setRequestOptions($url, $params);
        $this->setRequestHeaders();

        $response = curl_exec($this->ch);

        if ($response) {
            $this->parseResponse($response);
        } else {
            $this->error = curl_errno($this->ch).' - '.curl_error($this->ch);
        }

        curl_close($this->ch);

        return $this;
    }

    /**
     * Parse the response of the request
     *
     * @param string $response
     * @return void
     */
    private function parseResponse(string $response)
    {
        // Headers regex
        $pattern = '#HTTP/\d\.\d.*?$.*?\r\n\r\n#ims';

        // Extract headers from response
        preg_match_all($pattern, $response, $matches);
        $headers_string 	= array_pop($matches[0]);
        $headers 			= explode("\r\n", str_replace("\r\n\r\n", '', $headers_string));

        // Remove headers from the response body
        $this->responseBody	= str_replace($headers_string, '', $response);

        // Extract the version and status from the first header
        $version_and_status = array_shift($headers);
        preg_match('#HTTP/(\d\.\d)\s(\d\d\d)\s(.*)#', $version_and_status, $matches);
        $this->responseHeaders['Http-Version'] 	= $matches[1];
        $this->responseHeaders['Status-Code'] 	= $matches[2];
        $this->responseHeaders['Status'] 		= $matches[2].' '.$matches[3];

        // Convert headers into an associative array
        foreach ($headers as $header) {
            preg_match('#(.*?)\:\s(.*)#', $header, $matches);
            $this->responseHeaders[$matches[1]] = $matches[2];
        }
    }

    /**
     * Set Request Method
     *
     * @param string $method
     * @return void
     */
    private function setRequestMethod(string $method)
    {
        switch (strtoupper($method)) {
            case 'HEAD':
                curl_setopt($this->ch, CURLOPT_NOBODY, true);
                break;
            case 'GET':
                curl_setopt($this->ch, CURLOPT_HTTPGET, true);
                break;
            case 'POST':
                curl_setopt($this->ch, CURLOPT_POST, true);
                break;
            default:
                curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $method);
        }
    }

    /**
     * Set Request Options
     *
     * @param string $url
     * @param string $params
     * @return void
     */
    private function setRequestOptions(string $url, string $params)
    {
        curl_setopt($this->ch, CURLOPT_URL, $url);

        if (!empty($params)) {
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $params);
        }

        # Set some default CURL options
        curl_setopt($this->ch, CURLOPT_HEADER, true);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_USERAGENT, $this->agent);

        if ($this->followRedirects === true) {
            curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
        }

        if ($this->referrer !== null) {
            curl_setopt($this->ch, CURLOPT_REFERER, $this->referrer);
        }

        # Set any custom CURL options
        foreach ($this->options as $option => $value) {
            curl_setopt($this->ch, constant('CURLOPT_'.str_replace('CURLOPT_', '', strtoupper($option))), $value);
        }
    }

    /**
     * Set Request Headers
     *
     * @return void
     */
    private function setRequestHeaders()
    {
        $headers = [];

        foreach ($this->headers as $key => $value) {
            $headers[] = $key . ': ' . $value;
        }

        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);
    }
}