<?php

namespace Titan\Libraries\Http\Response;

use Titan\Kernel\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Response::class)->alias('response');
    }
}