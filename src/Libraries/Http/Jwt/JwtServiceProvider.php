<?php

namespace Titan\Libraries\Http\Jwt;

use Titan\Kernel\ServiceProvider;

class JwtServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Jwt::class)->alias('jwt');
    }
}