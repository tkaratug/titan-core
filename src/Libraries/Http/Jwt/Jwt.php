<?php

namespace Titan\Libraries\Http\Jwt;

use Exception;

class Jwt
{
    /** @var int */
    private $exp = 60;

    /** @var int */
    private $leeway = 0;

    /** @var string */
    private $secret = '';

    /** @var string */
    private $alg = '';

    /** @var array */
    private $algorithms = [
        'HS256' => 'SHA256',
        'HS512' => 'SHA512',
        'HS384' => 'SHA384'
    ];

    /**
     * Jwt constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        // Get config parameters for jwt
        $config         = config('auth.jwt');

        // Check whether the algorithm is valid
        if (!isset($this->algorithms[$config['alg']])) {
            throw new Exception('Algorithm not supported');
        }

        // Check whether the secret is defined
        if (empty($config['secret'])) {
            throw new Exception('Secret may not be empty');
        }

        $this->exp      = $config['exp'];
        $this->leeway   = $config['leeway'];
        $this->secret   = $config['secret'];
        $this->alg      = $this->algorithms[$config['alg']];
    }

    /**
     * Create JWT
     *
     * @param array $payload
     * @param array $head
     * @return string
     * @throws Exception
     */
    public function encode(array $payload, array $head = [])
    {
        $header = [
            'typ'   => 'JWT',
            'alg'   => $this->alg
        ];

        if (!empty($head)) {
            array_merge($head, $header);
        }

        $payload['exp'] = time() + $this->exp * 60;
        $payload['jti'] = uniqid(time());
        $payload['iat'] = time();

        $header         = $this->urlSafeBase64Encode($this->jsonEncode($header));
        $payload        = $this->urlSafeBase64Encode($this->jsonEncode($payload));
        $message        = $header . '.' . $payload;

        $signature      = $this->urlSafeBase64Encode($this->signature($message));

        return $header . '.' . $payload . '.' . $signature;
    }

    /**
     * Decode JWT
     *
     * @param string $token
     * @return object
     * @throws Exception
     */
    public function decode(string $token)
    {
        $jwt = explode('.', $token);

        if (count($jwt) != 3) {
            throw new Exception('Wrong number of segments');
        }

        list ($head64, $payload64, $sign64) = $jwt;

        if (null === ($header = $this->jsonDecode($this->urlSafeBase64Decode($head64)))) {
            throw new Exception('Invalid header encoding');
        }

        if (null === ($payload = $this->jsonDecode($this->urlSafeBase64Decode($payload64)))) {
            throw new Exception('Invalid claims encoding');
        }

        if (false === ($signature = $this->urlSafeBase64Decode($sign64))) {
            throw new Exception('Invalid signature encoding');
        }

        if (empty($header->alg)) {
            throw new Exception('Empty algorithm');
        }

        if (!in_array($header->alg, $this->algorithms)) {
            throw new Exception('Algorithm not supported');
        }

        if (!$this->verify("$head64.$payload64", $signature, $header->alg)) {
            throw new Exception('Signature verification failed');
        }

        if (isset($payload->nbf) && $payload->nbf > (time() + $this->leeway)) {
            throw new Exception('Cannot handle token prior to ' . date(\DateTime::ISO8601, $payload->nbf));
        }

        if (isset($payload->iat) && $payload->iat > (time() + $this->leeway)) {
            throw new Exception('Cannot handle token prior to ' . date(\DateTime::ISO8601, $payload->iat));
        }

        if (isset($payload->exp) && (time() - $this->leeway) >= $payload->exp) {
            throw new Exception('Expired token');
        }

        return $payload;
    }

    /**
     * Make Signature
     *
     * @param string $message
     * @return string
     */
    private function signature(string $message): string
    {
        return hash_hmac($this->alg, $message, $this->secret, true);
    }


    /**
     * Verify a signature with message and secret key
     *
     * @param string $message
     * @param string $signature
     * @param string $alg
     * @return bool
     * @throws Exception
     */
    private function verify(string $message, string $signature, string $alg): bool
    {
        $hash   = hash_hmac($alg, $message, $this->secret, true);

        if (function_exists('hash_equals')) {
            return hash_equals($signature, $hash);
        }

        $len    = min($this->safeStrLen($signature), $this->safeStrLen($hash));
        $status = 0;

        for ($i = 0; $i < $len; $i++) {
            $status |= (ord($signature[$i]) ^ ord($hash[$i]));
        }

        $status |= ($this->safeStrLen($signature) ^ $this->safeStrLen($hash));

        return ($status === 0);
    }


    /**
     * URL Safe Base64 Encode
     *
     * @param string $data
     * @return string
     */
    private function urlSafeBase64Encode(string $data): string
    {
        return str_replace('=', '', strtr(base64_encode($data), '+/', '-_'));
    }

    /**
     * URL Safe Base64 Decode
     *
     * @param string $data
     * @return string
     */
    private function urlSafeBase64Decode(string $data): string
    {
        $remainder  = strlen($data) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $data  .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($data, '-_', '+/'));
    }

    /**
     * Encode a PHP object|array into a JSON string
     *
     * @param object|array $data
     * @return string
     * @throws Exception
     */
    private function jsonEncode($data)
    {
        $json = json_encode($data);

        if (function_exists('json_last_error') && $errno = json_last_error()) {
            $this->handleJsonError($errno);
        } elseif ($json === 'null' && $data !== null) {
            throw new Exception('Null result with non-null input');
        }

        return $json;
    }

    /**
     * Decode a JSON string to a PHP object
     *
     * @param string $data
     * @return object
     * @throws Exception
     */
    private function jsonDecode(string $data)
    {
        if (version_compare(PHP_VERSION, '5.4.0', '>=') && !(defined('JSON_C_VERSION') && PHP_INT_SIZE > 4)) {
            $obj                    = json_decode($data, false, 512, JSON_BIGINT_AS_STRING);
        } else {
            $max_int_length         = strlen((string) PHP_INT_MAX) - 1;
            $json_without_bigints   = preg_replace('/:\s*(-?\d{'.$max_int_length.',})/', ': "$1"', $data);
            $obj                    = json_decode($json_without_bigints);
        }

        if (function_exists('json_last_error') && $errno = json_last_error()) {
            static::handleJsonError($errno);
        } elseif ($obj === null && $data !== 'null') {
            throw new Exception('Null result with non-null input');
        }

        return $obj;
    }

    /**
     * Helper to create a json error
     *
     * @param int $errno
     * @return void
     * @throws Exception
     */
    private function handleJsonError(int $errno)
    {
        $messages = [
            JSON_ERROR_DEPTH            => 'Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH   => 'Invalid or malformed JSON',
            JSON_ERROR_CTRL_CHAR        => 'Unexpected control character found',
            JSON_ERROR_SYNTAX           => 'Syntax error, malformed JSON',
            JSON_ERROR_UTF8             => 'Malformed UTF-8 characters'
        ];

        throw new Exception(
            isset($messages[$errno])
                ? $messages[$errno]
                : 'Unknown JSON error: ' . $errno
        );
    }

    /**
     * Get the number of bytes in cryptographic strings.
     *
     * @param string $str
     * @return int
     */
    private function safeStrLen(string $str): int
    {
        if (function_exists('mb_strlen')) {
            return mb_strlen($str, '8bit');
        }

        return strlen($str);
    }

}
