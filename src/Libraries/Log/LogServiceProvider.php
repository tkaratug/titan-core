<?php

namespace Titan\Libraries\Log;

use Titan\Kernel\ServiceProvider;

class LogServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Log::class)->alias('log');
    }
}