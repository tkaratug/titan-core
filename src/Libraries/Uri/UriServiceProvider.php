<?php

namespace Titan\Libraries\Uri;

use Titan\Kernel\ServiceProvider;

class UriServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Uri::class)->alias('uri');
    }
}