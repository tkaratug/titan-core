<?php

namespace Titan\Libraries\Uri;

interface UriGeneratorInterface
{
    /**
     * Generates a full path, e.g. "/dir/file"
     */
    const FULL_PATH = 0;

    /**
     * Generates a full URL, e.g. "http://example.com/dir/file"
     */
    const FULL_URL  = 1;
}