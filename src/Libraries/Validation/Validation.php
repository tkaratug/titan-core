<?php

namespace Titan\Libraries\Validation;

use Titan\Exception\ExceptionHandler;
use Titan\Libraries\Language\Language;

class Validation
{
    /**
     * Labels of fields
     *
     * @var array
     */
    protected $labels	    = [];

    /**
     * Validation rules
     *
     * @var array
     */
    protected $rules 	    = [];

    /**
     * Values to validate
     *
     * @var array
     */
    protected $data		    = [];

    /**
     * Custom rules
     *
     * @var array
     */
    protected $customRule   = [];

    /**
     * Error messages for custom rules
     *
     * @var array
     */
    protected $customErrors = [];

    /**
     * Error messages
     *
     * @var array
     */
    protected $errors 	    = [];

    /**
     * Language
     *
     * @var Language
     */
    private $language;

    /**
     * Validation constructor.
     *
     * @param Language $language
     */
    public function __construct(Language $language)
    {
        $this->language = $language;
    }

    /**
     * Define a validation rule
     *
     * @param string $field
     * @param string $label
     * @param string $rules
     * @return void
     */
    public function rule($field, $label, $rules)
    {
        $this->labels[$field] 	= $label;
        $this->rules[$field]	= $rules;
    }

    /**
     * Define validation rules
     *
     * @param array $rules
     * @param array $params
     * @return void
     */
    public function rules(array $rules, array $params = [])
    {
        foreach ($rules as $key => $value) {
            $this->rule($key, $value['label'], $value['rules']);

            if (!empty($params)) {
                $this->data[$key] = $params[$key];
            }
        }
    }

    /**
     * Define data to validate
     *
     * @param string $field
     * @param string $data
     * @return void
     */
    public function data($field, $data)
    {
        $this->data[$field] = $data;
    }

    /**
     * Define bulk data to validate
     *
     * @param array $data
     * @return void
     */
    public function bulkData(array $data)
    {
        foreach ($data as $key => $value) {
            $this->data[$key] = $value;
        }
    }

    /**
     * Define custom validation rule
     *
     * @param string $rule
     * @param callable $callback
     * @return bool
     * @throws ExceptionHandler
     */
    public function addRule(string $rule, callable $callback, $error = null) : bool
    {
        if (method_exists(__CLASS__, $rule) || isset($this->customRule[$rule])) {
            throw new ExceptionHandler('Error', 'Validation rule ' . $rule . ' already exists.');
        }

        $this->customRule[$rule]    = $callback;

        if (!empty($error)) {
            $this->customErrors[$rule]  = $error;
        }

        return true;
    }

    /**
     * Validate
     *
     * @return bool
     * @throws \Titan\Exception\ExceptionHandler
     */
    public function isValid() : bool
    {
        foreach ($this->rules as $field => $rules) {

            $rules = explode('|', $rules);

            if (in_array('nullable', $rules)) {
                $nullableFieldKey = array_search('nullable', $rules);
                unset($rules[$nullableFieldKey]);

                $nullable = true;
            } else {
                $nullable = false;
            }
            krsort($rules);
            foreach ($rules as $rule) {
                if (strpos($rule, ':')) {
                    $group  = explode(':', $rule);
                    list($method, $param) = $group;
                } else {
                    $method = $rule;
                }

                if (is_callable([$this, $method])) {
                    if ($method == 'matches') {
                        $result = $this->$method($this->data[$field], $this->data[$param]);
                    } else {
                        if ($nullable) {
                            $result = $this->$method($this->data[$field], $param) || $this->nullable($this->data[$field]);
                        } else {
                            $result = $this->$method($this->data[$field], $param);
                        }
                    }

                    if ($result === false) {
                        $this->errors[$field] = $this->language->translate('validation', $method . '_error', [
                            '%s' => $this->labels[$field],
                            '%t' => $param
                        ]);
                    }
                } else if (isset($this->customRule[$method])) {
                    if ($nullable === true && !empty($this->data[$field])) {
                        $result = call_user_func($this->customRule[$method], $field, $this->data[$field], $param);
                    } else if ($nullable !== true && empty($this->data[$field])) {
                        $result = call_user_func($this->customRule[$method], $field, $this->data[$field], $param);
                    }

                    if ($result === false) {
                        $this->errors[$field] = $this->customErrors[$method];
                    }
                } else {
                    throw new ExceptionHandler('Validation Error', "Validator method $method does not exist.");
                }
            }
        }

        return count($this->errors) > 0 ? false : true;
    }

    /**
     * Sanitizing data
     *
     * @param string|array $data
     * @return string|array
     */
    public function sanitize($data)
    {
        if (!is_array($data)) {
            return filter_var(trim($data), FILTER_SANITIZE_STRING);
        }

        foreach ($data as $key => $value) {
            $data[$key] = filter_var($value, FILTER_SANITIZE_STRING);
        }

        return $data;
    }

    /**
     * Return errors
     *
     * @return array
     */
    public function errors() : array
    {
        return $this->errors;
    }

    /**
     * Return errors as string
     *
     * @return string
     */
    public function errorMessages() : string
    {
        $response = '';

        foreach ($this->errors as $key => $value) {
            $response .= '<p>' . $value . '</p>';
        }

        return $response;
    }

    /**
     * Nullable field check
     *
     * @param string $data
     * @return bool
     */
    protected function nullable($data) : bool
    {
        return is_array($data) ? (empty($data) === true) : (trim($data) === '');
    }

    /**
     * Required field check
     *
     * @param string $data
     * @return bool
     */
    protected function required($data) : bool
    {
        return is_array($data) ? (empty($data) === false) : (trim($data) !== '');
    }

    /**
     * Numeric field check
     *
     * @param int $data
     * @return bool
     */
    protected function numeric($data) : bool
    {
        return is_numeric($data);
    }

    /**
     * Email validation
     *
     * @param string $email
     * @return bool
     */
    protected function email($email) : bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Minimum character check
     *
     * @param string $data
     * @param int $length
     * @return bool
     */
    protected function min_len($data, $length) : bool
    {
        return (strlen(trim($data)) < $length) === false;
    }

    /**
     * Maximum character check
     *
     * @param string $data
     * @param int $length
     * @return bool
     */
    protected function max_len($data, $length) : bool
    {
        return (strlen(trim($data)) > $length) === false;
    }

    /**
     * Exact length check
     *
     * @param string $data
     * @param int $length
     * @return bool
     */
    protected function exact_len($data, $length) : bool
    {
        return (strlen(trim($data)) == $length) !== false;
    }

    /**
     * Alpha character validation
     *
     * @param string $data
     * @return bool
     */
    protected function alpha($data) : bool
    {
        if (!is_string($data)) {
            return false;
        }

        return ctype_alpha($data);
    }

    /**
     * Alphanumeric character validation
     *
     * @param string $data
     * @return bool
     */
    protected function alpha_num($data) : bool
    {
        return ctype_alnum($data);
    }

    /**
     * Alpha-dash character validation
     *
     * @param string $data
     * @return bool
     */
    protected function alpha_dash($data) : bool
    {
        return (!preg_match("/^([-a-z0-9_-])+$/i", $data)) ? false : true;
    }

    /**
     * Alpha-space character validation
     *
     * @param string $data
     * @return bool
     */
    protected function alpha_space($data) : bool
    {
        return (!preg_match("/^([A-Za-z0-9- ])+$/i", $data)) ? false : true;
    }

    /**
     * Integer validation
     *
     * @param int $data
     * @return bool
     */
    protected function integer($data) : bool
    {
        return filter_var($data, FILTER_VALIDATE_INT);
    }

    /**
     * Boolean validation
     *
     * @param string $data
     * @return bool
     */
    protected function boolean($data) : bool
    {
        $acceptable = [true, false, 0, 1, '0', '1'];

        return in_array($data, $acceptable, true);
    }

    /**
     * Float validation
     *
     * @param string $data
     * @return bool
     */
    protected function float($data) : bool
    {
        return filter_var($data, FILTER_VALIDATE_FLOAT);
    }

    /**
     * URL validation
     *
     * @param string $url
     * @return bool
     */
    protected function valid_url($url) : bool
    {
        return filter_var($url, FILTER_VALIDATE_URL);
    }

    /**
     * IP validation
     *
     * @param string $ip
     * @return bool
     */
    protected function valid_ip($ip) : bool
    {
        return filter_var($ip, FILTER_VALIDATE_IP);
    }

    /**
     * IPv4 validation
     *
     * @param string $ip
     * @return bool
     */
    protected function valid_ipv4($ip) : bool
    {
        return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
    }

    /**
     * IPv6 validation
     *
     * @param string $ip
     * @return bool
     */
    protected function valid_ipv6($ip) : bool
    {
        return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);
    }

    /**
     * Credit card validation
     *
     * @param string $data
     * @return bool
     */
    protected function valid_cc($data) : bool
    {
        $number = preg_replace('/\D/', '', $data);

        if (function_exists('mb_strlen')) {
            $number_length = mb_strlen($number);
        } else {
            $number_length = strlen($number);
        }

        $parity = $number_length % 2;

        $total=0;

        for ($i=0; $i<$number_length; $i++) {
            $digit = $number[$i];

            if ($i % 2 == $parity) {
                $digit *= 2;

                if ($digit > 9) {
                    $digit -= 9;
                }
            }

            $total += $digit;
        }

        return $total % 10 == 0;
    }

    /**
     * Field must contain something
     *
     * @param string $data
     * @param string $part
     * @return bool
     */
    protected function contains($data, $part) : bool
    {
        return strpos($data, $part) !== false;
    }

    /**
     * Minimum value validation
     *
     * @param int $data
     * @param int $min
     * @return bool
     */
    protected function min_numeric($data, $min) : bool
    {
        return (is_numeric($data) && is_numeric($min) && $data >= $min) !== false;
    }

    /**
     * Maximum value validation
     *
     * @param int $data
     * @param int $max
     * @return bool
     */
    protected function max_numeric($data, $max) : bool
    {
        return (is_numeric($data) && is_numeric($max) && $data <= $max) !== false;
    }

    /**
     * Matched fields validation
     *
     * @param string $data
     * @param string $field
     * @return bool
     */
    protected function matches($data, $field) : bool
    {
        return ($data == $field) !== false;
    }
}
