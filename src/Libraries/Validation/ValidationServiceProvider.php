<?php

namespace Titan\Libraries\Validation;

use Titan\Kernel\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Validation::class)->alias('validation');
    }
}