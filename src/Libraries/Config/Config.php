<?php

namespace Titan\Libraries\Config;

use Titan\Kernel\Application;

class Config
{
    /**
     * Application
     *
     * @var Application
     */
    private $app;

    /**
     * Config constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Get config item
     *
     * @param string|null $params
     * @return mixed
     */
    public function get(string $params = null)
    {
        // Get config file
        $config = $this->app->get('config_params');

        // If params is null return config
        if (is_null($params)) {
            return $config;
        }

        // Explode items
        $keys = explode('.', $params);

        // Find the item in array
        foreach ($keys as $key) {
            $config = $config[$key];
        }

        // return the item
        return $config;
    }

}
