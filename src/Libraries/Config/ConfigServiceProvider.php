<?php

namespace Titan\Libraries\Config;

use Titan\Kernel\ServiceProvider;

class ConfigServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Config::class)->alias('config');
    }
}