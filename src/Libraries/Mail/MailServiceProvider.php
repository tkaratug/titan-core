<?php

namespace Titan\Libraries\Mail;

use Titan\Kernel\ServiceProvider;
use PHPMailer\PHPMailer\PHPMailer;

class MailServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Mail::class)->alias('mail');
    }
}