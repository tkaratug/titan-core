<?php

namespace Titan\Libraries\Mail;

use Titan\Libraries\Config\Config;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mail
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var PHPMailer
     */
    protected $mailer;

    /**
     * Mail constructor.
     *
     * @param Config $config
     * @param PHPMailer $mailer
     */
    public function __construct(Config $config, PHPMailer $mailer)
    {
        $this->mailer       = $mailer;
        $this->config       = $config->get('mail');

        $mailer->Host       = $this->config['server'];
        $mailer->Username   = $this->config['username'];
        $mailer->Password   = $this->config['userpass'];
        $mailer->Port       = $this->config['port'];
        $mailer->CharSet    = $this->config['charset'];

        $mailer->SMTPAuth   = true;
        $mailer->isSMTP();
        $mailer->isHTML(true);
    }

    /**
     * Set mail sender
     *
     * @param string $from
     * @return $this
     * @throws Exception
     */
    public function from(string $from)
    {
        $this->mailer->setFrom($from);

        return $this;
    }

    /**
     * Set receivers as cc
     *
     * @param array $cc
     * @return $this
     */
    public function cc(array $cc = [])
    {
        array_map(function ($mail) {
            $this->mailer->addCC($mail);
        }, $cc);

        return $this;
    }

    /**
     * Set receivers as bcc
     *
     * @param array $bcc
     * @return $this
     */
    public function bcc(array $bcc = [])
    {
        array_map(function ($mail) {
            $this->mailer->addBCC($mail);
        }, $bcc);

        return $this;
    }

    /**
     * Set receivers
     *
     * @param array $to
     * @return $this
     */
    public function to(array $to = [])
    {
        array_map(function ($mail) {
            $this->mailer->addAddress($mail);
        }, $to);

        return $this;
    }

    /**
     * Set reply to addresses
     *
     * @param string $email
     * @return $this
     */
    public function replyTo(string $email)
    {
        $this->mailer->addReplyTo($email);

        return $this;
    }

    /**
     * Set mail subject
     *
     * @param string $subject
     * @return $this
     */
    public function subject(string $subject)
    {
        $this->mailer->Subject = $subject;

        return $this;
    }

    /**
     * Set body of mail
     *
     * @param string $body
     * @return $this
     */
    public function body(string $body)
    {
        $this->mailer->Body = $body;

        return $this;
    }

    /**
     * Set alt body of mail
     *
     * @param string $altBody
     * @return $this
     */
    public function altBody(string $altBody)
    {
        $this->mailer->AltBody = $altBody;

        return $this;
    }

    /**
     * Add attachments to mail
     *
     * @param array $attachments
     * @return $this
     */
    public function attachment(array $attachments = [])
    {
        array_map(function ($file) {
            $this->mailer->addAttachment($file);
        }, $attachments);

        return $this;
    }

    /**
     * Get SMTP errors
     *
     * @return string
     */
    public function error()
    {
        return $this->mailer->ErrorInfo;
    }

    /**
     * Call actions of mailer class
     *
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public function __call(string $method, array $args = [])
    {
        return call_user_func_array(
            [$this->mailer, $method],
            $args
        );
    }
}