<?php

namespace Titan\Libraries\Cookie;

use Titan\Kernel\ServiceProvider;

class CookieServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Cookie::class)->alias('cookie');
    }
}