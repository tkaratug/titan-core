<?php

namespace Titan\Libraries\Benchmark;

use Titan\Kernel\ServiceProvider;

class BenchmarkServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Benchmark::class)->alias('benchmark');
    }
}