<?php

namespace Titan\Libraries\Session;

use Titan\Libraries\Config\Config;

class Session
{
    /**
     * Config parameters for session.
     *
     * @var mixed
     */
    private $config;

    /**
     * Session constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        // Get configuration params for session
        $this->config = $config->get('session');

        // Other config params
        $this->config += [
            'path'      => ini_get('session.cookie_path'),
            'domain'    => ini_get('session.cookie_domain'),
        ];

        // Initializing
        $this->init();
    }

    /**
     * Initialize session
     *
     * @return void
     */
    private function init()
    {
        if (!isset($_SESSION)) {

            // Setting cookie params and lifetime
            session_set_cookie_params(
                $this->config['lifetime'],
                $this->config['path'],
                $this->config['domain'],
                $this->config['cookie_secure'],
                $this->config['cookie_httponly']
            );

            // Enabling these protects against Session Fixation Attacks
            ini_set('session.use_only_cookies', $this->config['use_only_cookies']);
            ini_set('session.use_trans_sid', false);

            // Start session
            session_start();

            // Set session hash
            $this->set('session_hash', $this->generateHash());

            // Set initial session params
            $this->set('session_id', session_id());
            $this->set('ip_address', $_SERVER['REMOTE_ADDR']);
            $this->set('user_agent', $_SERVER['HTTP_USER_AGENT']);
            $this->set('last_activity', time());
        } else {
            if ($this->get('session_hash') != $this->generateHash()) {
                $this->destroy(false);
            }
        }
    }

    /**
     * Set session variable
     *
     * @param string|array $storage
     * @param mixed $content
     * @return void
     */
    public function set($storage, $content = null)
    {
        if (is_array($storage)) {
            foreach ($storage as $key => $value) {
                $_SESSION[$key] = $value;
            }
        } else {
            $_SESSION[$storage] = $content;
        }
    }

    /**
     * Get session variable
     *
     * @param string|null $storage
     * @return mixed
     */
    public function get(string $storage = null)
    {
        return is_null($storage) ? $_SESSION : $_SESSION[$storage];
    }

    /**
     * Check if session variable is exist
     *
     * @param string $storage
     * @return bool
     */
    public function has(string $storage) : bool
    {
        return isset($_SESSION[$storage]);
    }

    /**
     * Delete session
     *
     * @param string|null $storage
     * @return void
     */
    public function delete(string $storage = null)
    {
        if (is_null($storage)) {
            session_unset();
        } else {
            unset($_SESSION[$storage]);
        }
    }

    /**
     * Session destroy
     *
     * @param bool $destroy
     * @return void
     */
    public function destroy(bool $destroy = true)
    {
        session_unset();
        session_regenerate_id();

        if ($destroy === true) {
            session_destroy();
        }
    }

    /**
     * Set flash message
     *
     * @param mixed $message
     * @param string|null $url
     * @return void
     */
    public function setFlash($message, string $url = null)
    {
        $this->set('flash', $message);

        if (null !== $url) {
            header("Location: $url");
            exit();
        }
    }

    /**
     * Get flash message
     *
     * @return mixed|null
     */
    public function getFlash()
    {
        if ($this->has('flash')) {
            $flash = $this->get('flash');
            $this->delete('flash');
            return $flash;
        }

        return null;
    }

    /**
     * Check if the flash message exists
     *
     * @return bool
     */
    public function hasFlash() : bool
    {
        return $this->has('flash');
    }

    /**
     * Session Id
     *
     * @return string
     */
    public function id() : string
    {
        return session_id();
    }

    /**
     * Regenerate session_id
     *
     * @return void
     */
    public function regenerate()
    {
        session_regenerate_id();

        $this->set('session_id', session_id());
        $this->set('last_activity', time());
    }

    /**
     * Session destructor
     */
    public function __destruct()
    {
        session_write_close();
    }

    /**
     * Generate Hash for Hijacking Security
     *
     * @return string
     */
    private function generateHash() : string
    {
        if (array_key_exists('REMOTE_ADDR', $_SERVER) && array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
            return md5(sha1(md5($_SERVER['REMOTE_ADDR'] . $this->config['encryption_key'] . $_SERVER['HTTP_USER_AGENT'])));
        }

        return md5(sha1(md5($this->config['encryption_key'])));
    }

}