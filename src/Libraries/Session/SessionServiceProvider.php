<?php

namespace Titan\Libraries\Session;

use Titan\Kernel\ServiceProvider;

class SessionServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Session::class)->alias('session');
    }
}