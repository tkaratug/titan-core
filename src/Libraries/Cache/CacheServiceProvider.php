<?php

namespace Titan\Libraries\Cache;

use Titan\Kernel\ServiceProvider;

class CacheServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Cache::class)->alias('cache');
    }
}