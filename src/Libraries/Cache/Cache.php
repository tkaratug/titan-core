<?php

namespace Titan\Libraries\Cache;

use Titan\Kernel\Application;
use Titan\Exception\ExceptionHandler;

class Cache
{
    /**
     * Name of cache file
     *
     * @var string
     */
    private $filename;

    /**
     * Cache path
     *
     * @var string
     */
    private $path;

    /**
     * Cache file extension
     *
     * @var string
     */
    private $extension;

    /**
     * Expire time
     *
     * @var int
     */
    private $expire;

    /**
     * Application
     *
     * @var Application
     */
    private $app;

    /**
     * Cache constructor.
     *
     * @param Application $app
     * @throws \ReflectionException
     */
    public function __construct(Application $app)
    {
        $this->app          = $app;

        // Getting cache config items
        $config             = $app->resolve('config')->get('app.cache');

        // Initializing
        $this->path 		= $app->get('root_path') . DS . $config['path'];
        $this->extension 	= $config['extension'];
        $this->expire 		= $config['expire'];
        $this->filename     = $config['filename'];
    }

    /**
     * Write data into the cache file
     *
     * @param string $key
     * @param mixed $data
     * @param null $expiration
     * @throws ExceptionHandler
     */
    public function save(string $key, $data, $expiration = null)
    {
        if(null !== $expiration)
            $this->expire = $expiration;

        $storedData = [
            'time'		=> time(),
            'expire'	=> $this->expire,
            'data'		=> serialize($data)
        ];

        $cacheContent = $this->loadCache();

        if (is_array($cacheContent) === true) {
            $cacheContent[$key] = $storedData;
        } else {
            $cacheContent = [$key => $storedData];
        }

        $cacheContent = json_encode($cacheContent);
        file_put_contents($this->getCacheDir(), $cacheContent);
    }

    /**
     * Read data from the cache file
     *
     * @param string $key
     * @param string|null $filename
     * @return mixed|null
     * @throws ExceptionHandler
     */
    public function read(string $key, string $filename = null)
    {
        $cacheContent = $this->loadCache($filename);

        if (!isset($cacheContent[$key]['data'])) {
            return null;
        }

        return unserialize($cacheContent[$key]['data']);
    }

    /**
     * Delete an item from cache file
     *
     * @param string $key
     * @return void
     * @throws ExceptionHandler
     */
    public function delete(string $key)
    {
        $cacheContent = $this->loadCache();

        if (is_array($cacheContent)) {
            if (isset($cacheContent[$key])) {
                unset($cacheContent[$key]);
                $cacheContent = json_encode($cacheContent);
                file_put_contents($this->getCacheDir(), $cacheContent);
            } else {
                throw new ExceptionHandler("Error", "Key not found {" . $key . "}");
            }
        }
    }

    /**
     * Delete expired cached data
     *
     * @return int
     * @throws ExceptionHandler
     */
    public function deleteExpiredCache() : int
    {
        $counter = 0;
        $cacheContent = $this->loadCache();
        if (is_array($cacheContent)) {
            foreach ($cacheContent as $key => $value) {
                if ($this->isExpired($value['time'], $value['expire']) === true) {
                    unset($cacheContent[$key]);
                    $counter++;
                }
            }

            if($counter > 0) {
                $cacheContent = json_encode($cacheContent);
                file_put_contents($this->getCacheDir(), $cacheContent);
            }
        }
        return $counter;
    }

    /**
     * Delete all cached datas
     *
     * @throws ExceptionHandler
     */
    public function clear()
    {
        if (file_exists($this->getCacheDir())) {
            $file = fopen($this->getCacheDir(), 'w');
            fclose($file);
        }
    }

    /**
     * Check if cached data with $key exists
     *
     * @param string $key
     * @return bool
     * @throws ExceptionHandler
     */
    public function has(string $key) : bool
    {
        $this->deleteExpiredCache();
        if ($this->loadCache() != false) {
            $cacheContent = $this->loadCache();
            return isset($cacheContent[$key]['data']);
        }

        return false;
    }

    /**
     * Set cache filename
     *
     * @param $filename
     * @return $this
     */
    public function setFileName(string $filename)
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * Get cache filename
     *
     * @return string
     */
    public function getFileName() : string
    {
        return $this->filename;
    }

    /**
     * Set cache file path
     *
     * @param string $path
     * @return $this
     */
    public function setPath(string $path)
    {
        $this->path = $this->app->get('root_path') . DS . $path;
        return $this;
    }

    /**
     * Get cache file path
     *
     * @return string
     */
    public function getPath() : string
    {
        return $this->path;
    }

    /**
     * Set cache file extension
     *
     * @param $extension
     * @return $this
     */
    public function setExtension(string $extension)
    {
        $this->extension = $extension;
        return $this;
    }

    /**
     * Get cache file extension
     *
     * @return string
     */
    public function getExtension() : string
    {
        return $this->extension;
    }

    /**
     * Check if cache directory exists
     *
     * @return bool
     * @throws ExceptionHandler
     */
    private function checkCacheDir() : bool
    {
        if (!is_dir($this->getPath()) && !mkdir($this->getPath(), 0775, true)) {
            throw new ExceptionHandler("Error", "Unable to create cache directory." . $this->getPath());
        } elseif (!is_readable($this->getPath()) || !is_writable($this->getPath())) {
            if (!chmod($this->getPath(), 0775)) {
                throw new ExceptionHandler("Error", $this->getPath() . " directory must have read and write permissions.");
            }
        }

        return true;
    }

    /**
     * Get cache directory
     *
     * @param string|null $filename
     * @return bool|string
     * @throws ExceptionHandler
     */
    private function getCacheDir(string $filename = null)
    {
        if ($this->checkCacheDir() === true) {
            if (is_null($filename)) {
                $filename = preg_replace('/[^0-9a-z\.\_\-]/i', '', strtolower($this->getFileName()));
            }
            return $this->getPath() . '/' . $this->hashFile($filename) . $this->getExtension();
        }

        return false;
    }

    /**
     * Load cached file content
     *
     * @param string|null $filename
     * @return bool|mixed
     * @throws ExceptionHandler
     */
    private function loadCache(string $filename = null)
    {
        if ($this->getCacheDir() !== false) {
            if(file_exists($this->getCacheDir($filename))) {
                $file = file_get_contents($this->getCacheDir($filename));
                return json_decode($file, true);
            }

            return false;
        }

        return false;
    }

    /**
     * Chech if cached data expired
     *
     * @param int $time
     * @param int $expiration
     * @return bool
     */
    private function isExpired(int $time, int $expiration) : bool
    {
        if ($expiration !== 0) {
            $time_diff = time() - $time;

            if($time_diff > $expiration) {
                return true;
            }

            return false;
        }

        return false;
    }

    /**
     * Hash cache file name
     *
     * @param string $filename
     * @return string
     */
    private function hashFile(string $filename) : string
    {
        return md5($filename);
    }

}
