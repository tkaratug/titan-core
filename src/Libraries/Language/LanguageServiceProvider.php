<?php

namespace Titan\Libraries\Language;

use Titan\Kernel\ServiceProvider;

class LanguageServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Language::class)->alias('language');
    }
}