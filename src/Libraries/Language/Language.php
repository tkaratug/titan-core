<?php

namespace Titan\Libraries\Language;

use Titan\Kernel\Application;
use Titan\Exception\ExceptionHandler;

class Language
{
    /**
     * Locale in config
     *
     * @var string
     */
    private $locale;

    /**
     * Languages
     *
     * @var array
     */
    private $languages;

    /**
     * Language directory in application
     *
     * @var string
     */
    private $appLangDir;

    /**
     * Language directory in core
     *
     * @var string
     */
    private $sysLangDir;

    /**
     * Session
     *
     * @var object
     */
    private $session;

    /**
     * Config
     *
     * @var array
     */
    private $config;

    /**
     * Language constructor.
     *
     * @param Application $app
     * @throws \ReflectionException
     */
    public function __construct(Application $app)
    {
        $this->session          = $app->resolve('session');
        $this->config           = $app->resolve('config')->get('app');

        $this->appLangDir       = $app->get('app_path') . DS . 'Languages' . DS;
        $this->sysLangDir       = dirname(__DIR__, 2) . DS . 'Languages' . DS;

        $this->locale           = $this->config['locale'];
        $this->languages        = $this->config['languages'];
    }

    /**
     * Set active language
     *
     * @param string $language
     * @return mixed
     */
    public function set(string $language)
    {
        return $this->session->set('lang', $this->languages[$language]);
    }

    /**
     * Get active language
     *
     * @return array
     */
    public function get() : array
    {
        return $this->session->get('lang');
    }

    /**
     * Translate
     *
     * @param string $file
     * @param string $key
     * @param null $change
     * @return bool|mixed
     * @throws ExceptionHandler
     */
    public function translate(string $file, string $key, $change = null)
    {
        if (file_exists($this->appLangDir . $this->get()['info'] . DS . ucfirst($file) . '.php')) {
            $lang = require $this->appLangDir . $this->get()['info'] . DS . ucfirst($file) . '.php';
        } else if (file_exists($this->sysLangDir . $this->get()['info'] . DS . ucfirst($file) . '.php')) {
            $lang = require $this->sysLangDir . $this->get()['info'] . DS . ucfirst($file) . '.php';
        } else {
            throw new ExceptionHandler('File not found.', '<b>Language : </b> ' . $file);
        }

        if (isset($lang[$key])) {
            $str = $lang[$key];

            // Change special words
            if (!is_array($change)) {
                if (!empty($change)) {
                    return str_replace('%s', $change, $str);
                }
                return $str;
            } else {
                if (!empty($change)) {
                    $keys = [];
                    $vals = [];

                    foreach($change as $key => $value) {
                        $keys[] = $key;
                        $vals[] = $value;
                    }

                    return str_replace($keys, $vals, $str);
                }
                return $str;
            }

        }

        return false;
    }
}