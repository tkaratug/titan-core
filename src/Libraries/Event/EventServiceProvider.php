<?php

namespace Titan\Libraries\Event;

use Titan\Kernel\ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Event::class)->alias('event');
    }
}