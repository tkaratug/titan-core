<?php

namespace Titan\Libraries\Event;

use Titan\Kernel\Application;

class Event
{
    /**
     * Application
     *
     * @var Application
     */
    protected $app;

    /**
     * Event stack
     *
     * @var string
     */
    protected $stack = 'app';

    /**
     * Event constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Set event stack
     *
     * @param string $stack
     * @return $this
     */
    public function stack(string $stack)
    {
        $this->stack = $stack;

        return $this;
    }

    /**
     * Fire the listener
     *
     * @param $event
     * @throws \ReflectionException
     */
    public function fire($event)
    {
        // Get event-listener mappings from config
        $events = $this->app->resolve('config')->get('services.events.' . $this->stack);

        // Run handle method for each listener that belongs to the event
        foreach ($events[get_class($event)] as $listener) {
            $this->app->resolve($listener)->handle($event);
        }
    }
}
