<?php

namespace Titan\Libraries\Router;

use LogicException;
use Titan\Exception\NotFoundHttpException;
use Titan\Libraries\Http\Request\Request;

class Router
{
    private array $routes = [];

    private array $middlewares = [];

    private string $baseRoute = '/';

    private string $prefix = '';

    private string $namespace = '';

    private string $domain = '';

    private string $ip = '';

    private bool $ssl = false;

    private string $notFound = '';

    private array $groups = [];

    private int $groupped = 0;

    private array $namespaces = [
        'controllers'   => 'App\\Controllers',
        'middlewares'   => 'App\\Middlewares'
    ];

    private array $patterns = [
        '{num}'         => '([0-9]+)',
        '{alpha}'       => '([a-zA-Z_-]+)',
        '{all}'         => '([A-Za-z0-9_-]+)',
        '{any}'         => '([^/]+)'
    ];

    private array $current = [];

    private array $languages;

    private bool $matched = false;

    private Request $request;

    private RouteHandler $handler;

    private RouteParser $parser;

    /**
     * Router constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request      = $request;
        $this->handler      = new RouteHandler($request);
        $this->parser       = new RouteParser($request);
        $this->languages    = config('app.languages');
    }

    /**
     * Routing Groups
     *
     * @param callable $callback
     */
    public function group(callable $callback)
    {
        $this->groupped++;

        $this->groups[] = [
            'baseRoute'     => $this->baseRoute,
            'prefix'        => $this->prefix,
            'middlewares'   => $this->middlewares,
            'namespace'     => $this->namespace,
            'domain'        => $this->domain,
            'ip'            => $this->ip,
            'ssl'           => $this->ssl
        ];

        // Call the Callable
        call_user_func($callback);

        if ($this->groupped > 0) {
            $this->baseRoute    = $this->groups[$this->groupped-1]['baseRoute'];
            $this->prefix       = $this->groups[$this->groupped-1]['prefix'];
            $this->middlewares  = $this->groups[$this->groupped-1]['middlewares'];
            $this->namespace    = $this->groups[$this->groupped-1]['namespace'];
            $this->domain       = $this->groups[$this->groupped-1]['domain'];
            $this->ip           = $this->groups[$this->groupped-1]['ip'];
            $this->ssl          = $this->groups[$this->groupped-1]['ssl'];
        }

        $this->groupped--;

        if ($this->groupped <= 0) {
            // Reset Base Route
            $this->baseRoute    = '/';

            // Reset Prefix
            $this->prefix       = '';

            // Reset Middlewares
            $this->middlewares  = [];

            // Reset Namespace
            $this->namespace    = '';

            // Reset Domain
            $this->domain       = '';

            // Reset IP
            $this->ip           = '';

            // Reset SSL
            $this->ssl          = false;
        }
    }

    /**
     * Defining namespace
     *
     * @param string $namespace
     * @return $this
     */
    public function setNamespace(string $namespace): Router
    {
        // Set Namespace
        $this->namespace = $namespace;

        return $this;
    }

    /**
     * Defining middlewares
     *
     * @param array $middlewares
     * @return $this
     */
    public function middleware(array $middlewares): Router
    {
        foreach ($middlewares as $middleware) {
            $this->middlewares[$middleware] = [
                'callback' => $this->namespaces['middlewares'] . '\\' . ucfirst($middleware) . '@handle'
            ];
        }

        return $this;
    }

    /**
     * Defining prefix
     *
     * @param string $prefix
     * @return $this
     */
    public function prefix(string $prefix): Router
    {
        // Set Base Route
        $this->baseRoute = '/' . $prefix;

        // Set Prefix
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Defining domain
     *
     * @param string $domain
     * @return $this
     */
    public function domain(string $domain): Router
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Defining ip address
     *
     * @param string $ip
     * @return $this
     */
    public function ip(string $ip): Router
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Defining ssl
     *
     * @return $this
     */
    public function ssl(): Router
    {
        $this->ssl = true;

        return $this;
    }

    /**
     * @param string $method
     * @param string $uri
     * @param mixed $callback
     * @return void
     */
    public function route(string $method, string $uri, $callback): void
    {
        // Set Pattern
        $pattern = $this->handler->setPattern($uri, $this->baseRoute);

        // Set Closure
        if (is_callable($callback)) {
            $closure = $callback;
        } elseif (stripos($callback, '@') !== false) {
            if (!empty($this->namespace)) {
                $closure = $this->namespaces['controllers'] . '\\' . ucfirst($this->namespace) . '\\' . $callback;
            } else {
                $closure = $this->namespaces['controllers'] . '\\' . $callback;
            }
        } else {
            throw new LogicException("{$callback} is not callable.");
        }

        $routeArray = [
            'uri'       => $uri,
            'method'    => $method,
            'pattern'   => $pattern,
            'callback'  => $closure,
            'name'      => null,
        ];

        if (!empty($this->namespace)) {
            $routeArray['namespace']    = ucfirst($this->namespace);
        }

        if (!empty($this->prefix) || $this->prefix === '') {
            $routeArray['prefix']       = $this->prefix;
        }

        if (!empty($this->middlewares)) {
            $routeArray['middlewares']  = $this->middlewares;
        }

        if (!empty($this->domain)) {
            $routeArray['domain']       = $this->domain;
        }

        if (!empty($this->ip)) {
            $routeArray['ip']           = $this->ip;
        }

        if ($this->ssl) {
            $routeArray['ssl']          = $this->ssl;
        }

        $this->routes[] = $routeArray;
    }

    /**
     * Add a route using GET method
     *
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function get(string $pattern, $callback): Router
    {
        $this->route('GET', $pattern, $callback);

        return $this;
    }

    /**
     * Add a route using POST method
     *
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function post(string $pattern, $callback): Router
    {
        $this->route('POST', $pattern, $callback);

        return $this;
    }

    /**
     * Add a route using PATCH method
     *
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function patch(string $pattern, $callback): Router
    {
        $this->route('PATCH', $pattern, $callback);

        return $this;
    }

    /**
     * Add a route using DELETE method
     *
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function delete(string $pattern, $callback): Router
    {
        $this->route('DELETE', $pattern, $callback);

        return $this;
    }

    /**
     * Add a route using PUT method
     *
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function put(string $pattern, $callback): Router
    {
        $this->route('PUT', $pattern, $callback);

        return $this;
    }

    /**
     * Add a route using OPTIONS method
     *
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function options(string $pattern, $callback): Router
    {
        $this->route('OPTIONS', $pattern, $callback);

        return $this;
    }

    /**
     * @param array $methods
     * @param string $pattern
     * @param string|callable $callback
     * @return $this
     */
    public function match(array $methods, string $pattern, $callback): Router
    {
        foreach ($methods as $method) {
            $this->route(strtoupper($method), $pattern, $callback);
        }

        return $this;
    }

    /**
     * Set regular expression for parameters in the querystring
     *
     * @param $expressions
     * @return $this
     */
    public function where($expressions): Router
    {
        $routeKey   = array_search(end($this->routes), $this->routes);
        $pattern    = $this->parser->parseUri($this->routes[$routeKey]['prefix'] . $this->routes[$routeKey]['uri'], $expressions);
        $pattern    = '/' . implode('/', $pattern);

        foreach ($this->patterns as $key => $val) {
            $pattern = str_replace($key, $val, $pattern);
        }

        $pattern    = '/^' . str_replace('/', '\/', $pattern) . '$/';

        $this->routes[$routeKey]['pattern'] = $pattern;

        return $this;
    }

    /**
     * Set name for a route
     *
     * @param string $name
     * @return $this
     */
    public function name(string $name): Router
    {
        $routeKey = array_search(end($this->routes), $this->routes);
        $this->routes[$routeKey]['name'] = $name;

        return $this;
    }

    /**
     * Get all routes
     *
     * @return array
     */
    public function getRoutes() : array
    {
        return $this->routes;
    }

    /**
     * Set the 404 handling function
     *
     * @param $callback
     */
    public function set404($callback)
    {
        $this->notFound = $callback;
    }

    /**
     * Run routing
     *
     * @throws \ReflectionException
     */
    public function run()
    {
        // Get current url and language
        $current = $this->parser->getCurrentUri($this->languages);

        // If the language is not empty, set as current language
        if (!empty($current['language'])) {
            app('language')->set($current['language']);
        }

        foreach ($this->routes as $key => $val) {
            if (preg_match($val['pattern'], $current['url'], $params)) {
                $this->matched = true;
                $this->current = $val;

                // Checking domain
                $this->handler->checkDomain($val);

                // Checking IP
                $this->handler->checkIp($val);

                // Checking SSL
                $this->handler->checkSSL($val);

                // Checking request method
                $this->handler->checkMethod($val['method']);

                array_shift($params);

                // Checking middlewares
                if (array_key_exists('middlewares', $val)) {
                    $this->handler->runMiddlewares($val['middlewares']);
                }

                if (is_callable($val['callback'])) {
                    call_user_func_array($val['callback'], array_values($params));
                } else if (stripos($val['callback'], '@') !== false) {
                    try {
                        $this->handler->runController($val['callback'], $params);
                    } catch (NotFoundHttpException $e) {
                        $this->pageNotFound();
                    }
                }

                break;
            }
        }

        if (!$this->matched) {
            $this->pageNotFound();
        }
    }

    /**
     * Get url based on named route
     *
     * @param string $name
     * @param array $params
     * @param string $lang
     * @return ?string
     */
    public function getUrl(string $name, array $params = [], string $lang = ''): ?string
    {
        $pattern = $this->getPatternOfNamedRoute($name, $params);

        if (!empty($pattern)) {
            return !empty($lang) ? $this->parser->generateUrl($lang . '/' . $pattern) : $this->parser->generateUrl($pattern);
        }

        return null;
    }

    /**
     * Get path based on named route
     *
     * @param string $name
     * @param array $params
     * @param string $lang
     * @return string|null
     */
    public function getPath(string $name, array $params = [], string $lang = '')
    {
        $pattern = $this->getPatternOfNamedRoute($name, $params);

        if (!empty($pattern)) {
            return !empty($lang) ? '/' . $lang . '/' . $pattern : '/' . $pattern;
        }

        return null;
    }

    /**
     * Get current route
     *
     * @param string|null $param
     * @return array|mixed
     */
    public function currentRoute(string $param = null)
    {
        if (array_key_exists($param, $this->current)) {
            return $this->current[$param];
        }

        return $this->current;
    }

    /**
     * @param string $method
     * @param array $args
     * @return $this
     */
    public function __call(string $method, array $args = [])
    {
        if ($method == 'namespace') {
            $this->setNamespace($args[0]);
            return $this;
        }
    }

    /**
     * Get pattern of named route
     *
     * @param string $name
     * @param array $params
     * @return string|null
     */
    private function getPatternOfNamedRoute(string $name, array $params = [])
    {
        if (!empty($this->routes)) {
            foreach ($this->routes as $route) {
                if (array_key_exists('name', $route) && $route['name'] == $name) {
                    $uri = $route['uri'];
                    $pattern = $this->parser->parseUri($route['prefix'] . $uri, $params);
                    $pattern = implode('/', $pattern);
                    return $pattern;
                }
            }
            return null;
        }
        return null;
    }

    /**
     * Page not found redirection
     */
    private function pageNotFound()
    {
        if (!empty($this->notFound) && is_callable($this->notFound)) {
            call_user_func($this->notFound);
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }
}
