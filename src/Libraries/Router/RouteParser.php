<?php

namespace Titan\Libraries\Router;

use Titan\Libraries\Http\Request\Request;

class RouteParser
{
    protected Request $request;

    /**
     * RouteParser constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Parse url with parameters
     *
     * @param string $uri
     * @param array $expressions
     * @return array
     */
    public function parseUri(string $uri, array $expressions = []): array
    {
        $pattern = explode('/', ltrim($uri, '/'));
        foreach ($pattern as $key => $val) {
            if(preg_match('/[\[{\(].*[\]}\)]/U', $val, $matches)) {
                foreach ($matches as $match) {
                    $matchKey = substr($match, 1, -1);
                    if (array_key_exists($matchKey, $expressions)) {
                        $pattern[$key] = $expressions[$matchKey];
                    }
                }
            }
        }

        return $pattern;
    }

    /**
     * Get base path
     *
     * @return string
     */
    protected function getBasePath(): string
    {
        $scriptName = array_slice(explode('/', $this->request->getScriptName()), 0, -1);
        return implode('/', $scriptName) . '/';
    }

    /**
     * Get current uri
     *
     * @param array $languages
     * @return array
     */
    public function getCurrentUri(array $languages): array
    {
        // Get the current Request URI and remove rewrite base path from it
        $uri = substr($this->request->getRequestUri(), strlen($this->getBasePath()));

        // Don't take query params into account on the URL
        if (strstr($uri, '?')) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }

        // Remove trailing slash + enforce a slash at the start
        $current = '/' . trim($uri, '/');

        // Default language
        $language = null;

        $partsOfUrl = explode('/', $current);
        if (array_key_exists($partsOfUrl[1], $languages)) {
            $language   = $partsOfUrl[1];
            array_splice($partsOfUrl, 1, 1);
            $current    = implode('/', $partsOfUrl);
        }

        return [
            'url'       => $current,
            'language'  => $language
        ];
    }

    /**
     * Generates an absolute url
     *
     * @param ?string $url
     * @return string
     */
    public function generateUrl(?string $url): string
    {
        $protocol = $this->request->getScheme();

        if (is_null($url)) {
            return $protocol . "://" . $this->request->getHost();
        }

        return $protocol . "://" . rtrim($this->request->getHost(), '/') . '/' . $url;
    }
}