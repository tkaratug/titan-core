<?php

namespace Titan\Libraries\Router;

use Titan\Kernel\ServiceProvider;

class RouterServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Router::class)->alias('router');
    }
}