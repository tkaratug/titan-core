<?php

namespace Titan\Libraries\Router;

use ReflectionMethod;
use Titan\Exception\AccessDeniedHttpException;
use Titan\Exception\MethodNotAllowedHttpException;
use Titan\Exception\NotFoundHttpException;
use Titan\Libraries\Http\Request\Request;

class RouteHandler
{
    protected Request $request;

    /**
     * RouteHandler constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Run middlewares before enter.
     *
     * @param array $middlewares
     * @throws \ReflectionException
     */
    public function runMiddlewares(array $middlewares)
    {
        // Checking middlewares
        foreach ($middlewares as $middleware) {
            list($middlewareClass, $method) = explode('@', $middleware['callback']);

            if (class_exists($middlewareClass)) {
                // Create new controller instance with dependencies
                $injected   = $this->buildDependencies($middlewareClass);

                $action     = new ReflectionMethod($middlewareClass, $method);
                $parameters = $action->getParameters();
                $args       = [];

                foreach ($parameters as $parameter) {
                    if (isset($parameter->getClass()->name)) {
                        $class  = $parameter->getClass()->name;
                        $args[] = app($class);
                    }
                }

                $action->invokeArgs($injected, $args);
            }
        }
    }

    /**
     * Run controller
     *
     * @param string $matched
     * @param array $params
     * @throws \ReflectionException
     */
    public function runController(string $matched, array $params)
    {
        list($controller, $method) = explode('@', $matched);

        if (class_exists($controller)) {
            // Create new controller instance with dependencies
            $injected   = $this->buildDependencies($controller);

            $action     = new \ReflectionMethod($controller, $method);
            $parameters = $action->getParameters();
            $args       = [];
            $index      = 0;

            foreach ($parameters as $parameter) {
                if (isset($parameter->getClass()->name)) {
                    $class  = $parameter->getClass()->name;
                    $args[] = app($class);
                } else {
                    $args[] = $params[$index];
                    $index++;
                }
            }

            $action->invokeArgs($injected, $args);
        } else {
            throw new NotFoundHttpException('Page not found.');
        }
    }

    /**
     * Set the url pattern
     *
     * @param string $pattern
     * @param string $baseRoute
     * @return string
     */
    public function setPattern(string $pattern, string $baseRoute): string
    {
        if ($pattern == '/') {
            $pattern = $baseRoute . trim($pattern, '/');
        } else {
            if ($baseRoute == '/') {
                $pattern = $baseRoute . trim($pattern, '/');
            } else {
                $pattern = $baseRoute . $pattern;
            }
        }

        $pattern = preg_replace('/[\[{\(].*[\]}\)]/U', '([^/]+)', $pattern);
        $pattern = '/^' . str_replace('/', '\/', $pattern) . '$/';

        return $pattern;
    }

    /**
     * Check domain
     *
     * @param array $params
     */
    public function checkDomain(array $params): void
    {
        if (array_key_exists('domain', $params)) {
            if ($params['domain'] !== trim(str_replace('www.', '', $this->request->server('SERVER_NAME')), '/')) {
                throw new AccessDeniedHttpException('You don\'t have permission to access this page.');
            }
        }
    }

    /**
     * Check IP address
     *
     * @param array $params
     */
    public function checkIp(array $params): void
    {
        if (array_key_exists('ip', $params)) {
            if (is_array($params['ip'])) {
                if (!in_array($this->request->getIp(), $params['ip'])) {
                    throw new AccessDeniedHttpException('You don\'t have permission to access this page.');
                }
            } else {
                if ($this->request->getIp() !== $params['ip']) {
                    throw new AccessDeniedHttpException('You don\'t have permission to access this page.');
                }
            }
        }
    }

    /**
     * Check ssl
     *
     * @param array $params
     */
    public function checkSSL(array $params): void
    {
        if (array_key_exists('ssl', $params) && $params['ssl'] === true) {
            if ($this->request->getScheme() !== 'https') {
                throw new AccessDeniedHttpException('This page requires private connection (https).');
            }
        }
    }

    /**
     * Check request method
     *
     * @param string $method
     */
    public function checkMethod(string $method): void
    {
        // Checking request method
        if (!$this->request->isMethod($method)) {
            throw new MethodNotAllowedHttpException(
                [$method],
                "Request method ({$this->request->method()}) is not allowed for this page."
            );
        }
    }

    /**
     * Create new controller instance with dependencies
     *
     * @param string $controller
     * @return object
     * @throws \ReflectionException
     */
    protected function buildDependencies(string $controller): object
    {
        if (method_exists($controller, '__construct')) {
            $reflector      = new \ReflectionClass($controller);
            $constructor    = $reflector->getConstructor();
            $parameters     = $constructor->getParameters();
            $arguments      = [];

            foreach ($parameters as $parameter) {
                if (isset($parameter->getClass()->name)) {
                    $class          = $parameter->getClass()->getName();
                    $arguments[]    = $this->app->resolve($class);
                }
            }

            return $reflector->newInstanceArgs($arguments);
        }

        return new $controller;
    }
}
