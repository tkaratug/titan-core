<?php

namespace Titan\Libraries\Load;

use Titan\Kernel\ServiceProvider;

class LoadServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Load::class)->alias('load');
    }
}