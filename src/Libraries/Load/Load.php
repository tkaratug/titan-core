<?php

namespace Titan\Libraries\Load;

use Titan\Kernel\Application;
use Titan\Exception\ExceptionHandler;

class Load
{
    /**
     * Application
     *
     * @var Application
     */
    private $app;

    /**
     * Load constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Load view
     *
     * @param string $file
     * @param array $data
     * @throws ExceptionHandler
     * @return mixed
     */
    public function view(string $file, array $data = [])
    {
        $filePath = $this->app->get('view_path') . DS . $file . '.php';

        if (file_exists($filePath)) {
            extract($data);
            return require $filePath;
        }

        throw new ExceptionHandler('File not found.', '<b>View : </b>' . $file);
    }

    /**
     * Load config file
     *
     * @param string $file
     * @return mixed
     * @throws ExceptionHandler
     */
    public function config(string $file)
    {
        $filePath = $this->app->get('config_path') . DS . ucfirst($file) . '.php';
        if (file_exists($filePath)) {
            return require $filePath;
        }

        throw new ExceptionHandler('File not found.', '<b>Config : </b>' . ucfirst($file));
    }

    /**
     * Load file
     *
     * @param string $file
     * @return mixed
     * @throws ExceptionHandler
     */
    public function file(string $file)
    {
        if (file_exists($file)) {
            return require $file;
        }

        throw new ExceptionHandler('File not found.', '<b>File : </b>' . $file);
    }

}