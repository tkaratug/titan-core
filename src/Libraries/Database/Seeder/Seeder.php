<?php

namespace Titan\Libraries\Database\Seeder;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Opis\Database\Schema\CreateTable;

class Seeder
{
    /**
     * Application
     *
     * @var Application
     */
    protected $app;

    /**
     * Loader
     *
     * @var Load
     */
    protected $load;

    /**
     * Seeder Config
     *
     * @var array
     */
    protected $config;

    /**
     * Database
     *
     * @var DB
     */
    protected $db;

    /**
     * Seeder constructor.
     *
     * @throws \ReflectionException
     */
    public function __construct()
    {
        $this->app          = \Titan\Kernel\Facade::getFacadeApplication();
        $this->config       = $this->app->resolve('config')->get('database.seeder');
        $this->load         = $this->app->resolve('load');
        $this->db           = $this->app->resolve('db');
    }

    /**
     * Run seeders
     *
     * @param string $source
     * @return string
     */
    public function run(InputInterface $input, OutputInterface $output)
    {
        if ($handle = opendir($this->config['path'])) {

            $source = $input->getOption('source');

            if ($source == 'all') {

                // Sort all migrations by name ascending and get them all
                $files = $this->sortFilesByName($handle);

                foreach ($files as $entry) {
                    // Remove the extension
                    $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $entry);

                    // Get the class name
                    $className  = substr($withoutExt, 18);

                    // Check if the seeder table exists and if not, create it
                    if (!$this->checkSeederTable()) {
                        $this->createSeederTable();
                    }

                    // Check if the seeder has already run
                    if (!$this->checkSeederVersion($withoutExt)) {
                        $this->load->file($this->config['path'] . '/' . $entry);
                        $class  = $this->config['namespace'] . $className;
                        $seeder = new $class();

                        try {
                            $seeder->up();
                            $this->saveSeederLog($withoutExt);
                            $output->writeln("<fg=green>$withoutExt has run successfully</>");
                        } catch (\Exception $e) {
                            $seeder->down();
                            $output->writeln("<fg=red>An error has occured while running the seeder : $withoutExt\n{$e->getMessage()}</>");
                        }

                    } else {
                        $output->writeln("<fg=red>$withoutExt has already run before</>");
                    }
                }
            } else {
                if (file_exists($this->config['path'] . '/' . $source . '.php')) {
                    // Check if the seeder table exists and if not, create it
                    if (!$this->checkSeederTable()) {
                        $this->createSeederTable();
                    }

                    // Check if the seeder has already run
                    if (!$this->checkSeederVersion($source)) {
                        $this->load->file($this->config['path'] . '/' . $source . '.php');
                        $className  = substr($source, 18);
                        $class      = $this->config['namespace'] . $className;
                        $seeder     = new $class();

                        try {
                            $seeder->up();
                            $this->saveSeederLog($source);
                            $output->writeln("<fg=green>$source has run successfully</>");
                        } catch (\Exception $e) {
                            $seeder->down();
                            $output->writeln("<fg=red>An error has occured while running the seeder : $source\n{$e->getMessage()}</>");
                        }

                    } else {
                        $output->writeln("<fg=red>$source has already run before</>");
                    }
                } else {
                    $output->writeln("<fg=yellow>Seeder $source not found.</>");
                }
            }

            closedir($handle);
        }
    }

    /**
     * Sort and get files in directory
     *
     * @param $dir
     * @return array
     */
    private function sortFilesByName($dir)
    {
        $files = [];

        while (false !== ($entry = readdir($dir))) {
            if ($entry != "." && $entry != ".." && substr(strrchr($entry, '.'), 1) == 'php') {
                $files[] = $entry;
            }
        }

        sort($files);

        return $files;
    }

    /**
     * Check if the seeder table exist
     */
    private function checkSeederTable()
    {
        return $this->db->schema()->hasTable('seeders');
    }

    /**
     * Create seeder table
     *
     * @return mixed
     */
    private function createSeederTable()
    {
        return
            $this->db->schema()->create('seeders', function(CreateTable $table){
                $table->integer('id')->autoincrement()->primary();
                $table->string('version')->notNull();
                $table->timestamp('created_at')->notNull();
            });
    }

    /**
     * Check seeder version
     *
     * @param string $version
     * @return mixed
     */
    private function checkSeederVersion(string $version)
    {
        return $this->db->from('seeders')->where('version')->eq($version)->select()->first();
    }

    /**
     * Save seeder log
     *
     * @param string $version
     * @return mixed
     */
    private function saveSeederLog(string $version)
    {
        return $this->db->insert([
            'version' => $version,
            'created_at' => date('Y-m-d H:i:s')
        ])->into('seeders');
    }
}