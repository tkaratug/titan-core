<?php

namespace Titan\Libraries\Database\Seeder;

use Titan\Kernel\ServiceProvider;

class SeederServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->bind(Seeder::class)->alias('seeder');
    }
}