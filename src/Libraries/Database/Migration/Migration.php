<?php

namespace Titan\Libraries\Database\Migration;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Opis\Database\Schema\CreateTable;

class Migration
{
    /**
     * Application
     *
     * @var Application
     */
    protected $app;

    /**
     * Loader
     *
     * @var Load
     */
    protected $load;

    /**
     * Migration config
     *
     * @var array
     */
    protected $config;

    /**
     * Database
     *
     * @var DB
     */
    protected $db;

    /**
     * Last batch
     *
     * @var int
     */
    protected $batch;

    /**
     * Migration constructor.
     *
     * @throws \ReflectionException
     */
    public function __construct()
    {
        $this->app          = \Titan\Kernel\Facade::getFacadeApplication();
        $this->config       = $this->app->resolve('config')->get('database.migration');
        $this->load         = $this->app->resolve('load');
        $this->db           = $this->app->resolve('db');
    }

    /**
     * Run migrations
     */
    public function run(InputInterface $input, OutputInterface $output)
    {
        // Set last batch
        $this->batch = $this->lastBatch();

        if ($handle = opendir($this->config['path'])) {

            // Source of migration
            $source = $input->getOption('source');

            if ($source == 'all') {

                // If fresh option is requested
                if ($input->getOption('fresh') !== false) {
                    // Truncate migrations table
                    if ($this->db->schema()->hasTable('migrations')) {
                        $this->db->schema()->truncate('migrations');
                    }

                    // Get all tables
                    $tables = $this->db->schema()->getTables(true);

                    // Set foreign key checks to 0
                    $this->db->getConnection()->query("SET FOREIGN_KEY_CHECKS = 0");

                    // Drop all tables
                    foreach ($tables as $table) {
                        $this->db->schema()->drop($table);
                    }

                    // Set foreign key checks to 1
                    $this->db->getConnection()->query("SET FOREIGN_KEY_CHECKS = 1");
                }

                // Sort all migrations by name ascending and get them all
                $files = $this->sortFilesByName($handle);

                foreach ($files as $entry) {
                    // Remove the extension
                    $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $entry);

                    // Get the class name
                    $className  = substr($withoutExt, 18);

                    // Check if the migration table exists and if not, create it
                    if (!$this->checkMigrationTable()) {
                        $this->createMigrationTable();
                    }

                    // Check if the migration has already run
                    if (!$this->checkMigrationVersion($withoutExt)) {
                        $this->load->file($this->config['path'] . '/' . $entry);
                        $class = $this->config['namespace'] . $className;
                        $migration = new $class();

                        try {
                            $migration->up();
                            $this->saveMigrationLog($withoutExt);
                            $output->writeln("<fg=green>$withoutExt has run successfully</>");
                        } catch (\Exception $e) {
                            $migration->down();
                            $output->writeln("<fg=red>An error has occured while running the migration : $withoutExt\n{$e->getMessage()}</>");
                        }

                    } else {
                        $output->writeln("<fg=red>$withoutExt has already run before</>");
                    }
                }
            } else {
                if (file_exists($this->config['path'] . '/' . $source . '.php')) {
                    // Check if the migration table exists and if not, create it
                    if (!$this->checkMigrationTable()) {
                        $this->createMigrationTable();
                    }

                    // Check if the migration has already run
                    if (!$this->checkMigrationVersion($source)) {
                        $this->load->file($this->config['path'] . '/' . $source . '.php');
                        $className  = substr($source, 18);
                        $class      = $this->config['namespace'] . $className;
                        $migration  = new $class();

                        try {
                            $migration->up();
                            $this->saveMigrationLog($source);
                            $output->writeln("<fg=green>$source has run successfully</>");
                        } catch (\Exception $e) {
                            $migration->down();
                            $output->writeln("<fg=red>An error has occured while running the migration : $source\n{$e->getMessage()}</>");
                        }

                    } else {
                        $output->writeln("<fg=red>$source has already run before</>");
                    }
                } else {
                    $output->writeln("<fg=yellow>Migration $source not found.</>");
                }
            }

            closedir($handle);
        }
    }

    /**
     * Rollback the last batch of migrations
     *
     * @param OutputInterface $output
     * @return mixed
     */
    public function rollback(OutputInterface $output)
    {
        // Check if the migration table exists and if not, create it
        if (!$this->checkMigrationTable()) {
            return $output->writeln("<fg=red>Rollback has failed. 'migrations' table does not exist.</>");
        }

        // Set last batch
        $this->batch = $this->lastBatch();

        // Get migrations to roll back
        $migrations = $this->db->from('migrations')->where('batch')->is($this->batch)->select()->all();

        // Disable foreign key checks
        $this->db->command('SET FOREIGN_KEY_CHECKS = 0');

        foreach ($migrations as $migration) {
            $this->load->file($this->config['path'] . '/' . $migration->version . '.php');
            $file       = explode('_', $migration->version);
            $source     = end($file);
            $class      = $this->config['namespace'] . $source;
            $object     = new $class();

            try {
                $object->down();
                $this->deleteMigrationLog($migration->id);
                $output->writeln("<fg=green>$source has been rolled back successfully</>");
            } catch (\Exception $e) {
                $output->writeln("<fg=red>An error has occured while rolling back the migration : $source\n{$e->getMessage()}</>");
            }
        }

        // Enable foreign key checks
        $this->db->command('SET FOREIGN_KEY_CHECKS = 1');
    }

    /**
     * Get the last batch
     *
     * @return int
     * @throws ExceptionHandler
     */
    private function lastBatch()
    {
        // If the migration table exists, get the last batch
        if ($this->checkMigrationTable()) {
            // Get last run migration
            $lastBatch = $this->db->from('migrations')->orderBy('batch', 'desc')->limit(1)->select('batch')->first();

            // If there is no migration, return 0
            if (empty($lastBatch)) {
                return 0;
            }

            return $lastBatch->batch;
        }

        return 0;
    }

    /**
     * Sort and get files in directory
     *
     * @param $dir
     * @return array
     */
    private function sortFilesByName($dir)
    {
        $files = [];

        while (false !== ($entry = readdir($dir))) {
            if ($entry != "." && $entry != ".." && substr(strrchr($entry, '.'), 1) == 'php') {
                $files[] = $entry;
            }
        }

        sort($files);

        return $files;
    }

    /**
     * Check if the migration table exist
     */
    private function checkMigrationTable()
    {
        return $this->db->schema()->hasTable('migrations');
    }

    /**
     * Create migration table
     *
     * @return mixed
     */
    private function createMigrationTable()
    {
        return
            $this->db->schema()->create('migrations', function(CreateTable $table){
                $table->integer('id')->autoincrement()->primary();
                $table->string('version')->notNull();
                $table->integer('batch')->notNull();
                $table->timestamp('created_at')->notNull();
            });
    }

    /**
     * Check migration version
     *
     * @param string $version
     * @return mixed
     */
    private function checkMigrationVersion(string $version)
    {
        return $this->db->from('migrations')->where('version')->eq($version)->select()->first();
    }

    /**
     * Save migration log
     *
     * @param string $version
     * @return mixed
     */
    private function saveMigrationLog(string $version)
    {
        return $this->db->insert([
            'version' => $version,
            'created_at' => date('Y-m-d H:i:s'),
            'batch' => $this->batch + 1
        ])->into('migrations');
    }

    /**
     * Delete migration log
     *
     * @param int $id
     * @return mixed
     */
    private function deleteMigrationLog(int $id)
    {
        return $this->db->from('migrations')->where('id')->is($id)->delete();
    }
}