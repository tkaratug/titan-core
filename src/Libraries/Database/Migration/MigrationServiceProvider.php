<?php

namespace Titan\Libraries\Database\Migration;

use Titan\Kernel\ServiceProvider;

class MigrationServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->bind(Migration::class)->alias('migration');
    }
}