<?php

namespace Titan\Libraries\Database;

class Model
{
    /**
     * Model collection
     *
     * @var array
     */
    private $modelCollection = [];

    /**
     * Return instance of model
     *
     * @param string $model
     * @return mixed
     * @throws \ReflectionException
     */
    public function use(string $model)
    {
        if (!array_key_exists($model, $this->modelCollection)) {
            $this->modelCollection[$model] = new $model();
        }

        return $this->modelCollection[$model];
    }
}