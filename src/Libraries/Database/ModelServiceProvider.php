<?php

namespace Titan\Libraries\Database;

use Titan\Kernel\ServiceProvider;

class ModelServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->bind(Model::class)->alias('model');
    }
}