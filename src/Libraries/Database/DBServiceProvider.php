<?php

namespace Titan\Libraries\Database;

use Titan\Kernel\ServiceProvider;

class DBServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(DB::class)->alias('db');
    }
}