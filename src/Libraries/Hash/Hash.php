<?php

namespace Titan\Libraries\Hash;

class Hash implements HashInterface
{
    /**
     * Default crypt cost factor
     *
     * @var int
     */
    protected $cost = 10;

    /**
     * Hash the given value
     *
     * @param string $value
     * @param array $options
     * @return string
     */
    public function make(string $value, array $options = [])
    {
        if (!array_key_exists('cost', $options)) {
            $options['cost'] = $this->cost;
        }

        return password_hash($value, PASSWORD_DEFAULT, $options);
    }

    /**
     * Check the given value against a hash
     *
     * @param string $value
     * @param string $hashedValue
     * @return bool
     */
    public function check(string $value, string $hashedValue)
    {
        if (strlen($hashedValue) === 0) {
            return false;
        }

        return password_verify($value, $hashedValue);
    }

    /**
     * Check if the given value has been hashed using the given options
     *
     * @param string $hashedValue
     * @param array $options
     * @return bool
     */
    public function needsRehash(string $hashedValue, array $options = [])
    {
        if (!array_key_exists('cost', $options)) {
            $options['cost'] = $this->cost;
        }

        return password_needs_rehash($hashedValue, PASSWORD_DEFAULT, $options);
    }
}