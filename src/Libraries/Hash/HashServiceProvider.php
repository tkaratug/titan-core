<?php

namespace Titan\Libraries\Hash;

use Titan\Kernel\ServiceProvider;

class HashServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Hash::class)->alias('hash');
    }
}