<?php

namespace Titan\Libraries\View;

use Titan\Kernel\Application;
use Titan\Exception\ExceptionHandler;
use Windwalker\Edge\Cache\EdgeFileCache;
use Windwalker\Edge\Edge;
use Windwalker\Edge\Loader\EdgeFileLoader;

class View
{
    /**
     * Application
     *
     * @var Application
     */
    private $app;

    /**
     * Selected theme
     *
     * @var string|null
     */
    private $theme  = null;

    /**
     * Common parameters
     *
     * @var array
     */
    private $common = [];

    /**
     * View constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app  = $app;
    }

    /**
     * Get rendered file content
     *
     * @param string $file
     * @param array $vars
     * @param bool $cache
     * @return string
     * @throws \Titan\Exception\UndefinedKeyException
     * @throws \Windwalker\Edge\Exception\EdgeException
     */
    public function content(string $file, array $vars = [], $cache = true)
    {
        $paths  = [
            $this->app->get('view_path')
        ];

        $loader = new EdgeFileLoader($paths);
        $loader->addFileExtension('.blade.php');

        if ($cache) {
            $cachePath  = $this->app->get('storage_path') . DS . 'cache';
            $edge       = new Edge(new EdgeFileLoader($paths), null, new EdgeFileCache($cachePath));
        } else {
            $edge       = new Edge($loader);
        }

        // Merging parameters with vars
        $parameters     = array_merge($vars, $this->common);

        $content = $edge->render($file, $parameters);

        if (!empty($this->theme)) {
            $content = $edge->render($this->theme . $file, $parameters);
        }

        return $content;
    }

    /**
     * Render view file
     *
     * @param string $file
     * @param array $vars
     * @param bool $cache
     * @return void
     */
    public function render(string $file, array $vars = [], $cache = true)
    {
        // Merging parameters with vars
        $parameters     = array_merge($vars, $this->common);

        echo $this->content($file, $parameters, $cache);
    }

    /**
     * Set active theme
     *
     * @param string $theme
     * @return $this
     * @throws ExceptionHandler
     */
    public function theme(string $theme)
    {
        if (file_exists($this->app->get('view_path') . DS . $theme)) {
            $this->theme = $theme;
        } else {
            throw new ExceptionHandler('Error', 'Template directory is not found : { '. $theme .' }');
        }

        return $this;
    }

    /**
     * Set common parameters
     *
     * @param array $params
     */
    public function common($params = [])
    {
        $this->common = $params;
    }
}
