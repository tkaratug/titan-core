<?php

namespace Titan\Libraries\View;

use Titan\Kernel\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(View::class)->alias('view');
    }
}