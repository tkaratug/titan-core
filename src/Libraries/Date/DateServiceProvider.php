<?php

namespace Titan\Libraries\Date;

use Titan\Kernel\ServiceProvider;

class DateServiceProvider extends ServiceProvider
{
    /**
     * Register the library to the container
     */
    public function register()
    {
        $this->app->singleton(Date::class)->alias('date');
    }
}