<?php

namespace Titan\Facades;

use Titan\Kernel\Facade;

class Auth extends Facade
{
    /**
     * Get the registered name of the component
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'auth';
    }
}