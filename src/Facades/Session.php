<?php

namespace Titan\Facades;

use Titan\Kernel\Facade;

class Session extends Facade
{
    /**
     * Get the registered name of the component
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'session';
    }
}