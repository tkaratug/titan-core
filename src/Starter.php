<?php
/**
 * Titan Mini Framework
 * --
 * Simple and Modern Web Application Framework
 *
 * @author  Turan Karatuğ - <tkaratug@hotmail.com.tr>
 * @web     <http://www.titanphp.com>
 * @docs    <http://docs.titanphp.com>
 * @github  <http://github.com/tkaratug/titanframework>
 * @license The MIT License (MIT) - <http://opensource.org/licenses/MIT>
 */

use Titan\Kernel\Application;

/*
|-----------------------------------------------------------------------
| Titan Framework Version Number
|-----------------------------------------------------------------------
*/
define('VERSION', env('VERSION', '3.0.0'));

/*
|-----------------------------------------------------------------------
| Application Environment
|-----------------------------------------------------------------------
|
| Defining the application environment. ['dev', 'test', 'prod']
|
*/
define('APP_ENV', env('APP_ENV', 'dev'));

/*
|-----------------------------------------------------------------------
| Directory Separator
|-----------------------------------------------------------------------
|
| Defining directory separator
|
 */
define('DS', DIRECTORY_SEPARATOR);

/*
|-----------------------------------------------------------------------
| Timezone
|-----------------------------------------------------------------------
|
| Setting defalut timezone
|
 */
date_default_timezone_set(env('TIMEZONE', 'UTC'));

/*
|-----------------------------------------------------------------------
| Prepare Titan Kernel
|-----------------------------------------------------------------------
|
| Building structure and registering service providers.
|
*/
$app = new Application(APP_ENV);

/*
|-----------------------------------------------------------------------
| Run Titan
|-----------------------------------------------------------------------
|
| Awesome!
|
*/
$request = new \Titan\Libraries\Http\Request\Request();
$app->run($request);