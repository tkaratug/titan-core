<?php

namespace Titan\Console;

use Symfony\Component\Console\Application as ConsoleApp;
use Titan\Kernel\Application as TitanApp;

class Command
{
    protected ConsoleApp $consoleApp;

    protected TitanApp $titanApp;

    /**
     * Command constructor.
     *
     * @param ConsoleApp $consoleApp
     * @param TitanApp $titanApp
     */
    public function __construct(ConsoleApp $consoleApp, TitanApp $titanApp)
    {
        $this->consoleApp   = $consoleApp;
        $this->titanApp     = $titanApp;
        $this->generate($this->titanApp->resolve('config')->get('commands'));
    }

    /**
     * Run commands
     *
     * @return void
     */
    public function run()
    {
        $this->consoleApp->run();
        exit;
    }

    /**
     * Generate all commands
     *
     * @param array $commands
     */
    private function generate(array $commands)
    {
        foreach ($commands as $key => $val) {
            $this->consoleApp->add($this->titanApp->resolve($key));
        }
    }
}