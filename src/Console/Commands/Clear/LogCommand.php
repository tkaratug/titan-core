<?php

namespace Titan\Console\Commands\Clear;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Titan\Kernel\Application;

class LogCommand extends Command
{
    private Application $app;

    /**
     * LogCommand constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct();
        $this->app = $app;
    }

    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('clear:log')
             ->setDescription('Clear all log files.')
             ->setHelp('This command makes you to clear all log files.');
    }

    /**
     * Execute the command
     *
     * @param InputInterface input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        array_map('unlink', glob($this->app->get('storage_path') . '/log/*'));
        $output->writeln('<info>Success</info> Log files deleted successfully.');

        return Command::SUCCESS;
    }
}