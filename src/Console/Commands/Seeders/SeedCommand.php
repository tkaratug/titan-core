<?php

namespace Titan\Console\Commands\Seeders;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Titan\Kernel\Application;

class SeedCommand extends Command
{
    protected Application $app;

    /**
     * MigrateCommand constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct();
        $this->app = $app;
    }

    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('db:seed')
             ->setDescription('Run all seeders.')
             ->setHelp("This command makes you to run all seeders...")
             ->addOption('source', 's', InputOption::VALUE_REQUIRED, 'Specific seeder name', 'all');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->app->resolve('seeder')->run($input, $output);
        return Command::SUCCESS;
    }
}