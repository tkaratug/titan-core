<?php

namespace Titan\Console\Commands\Make;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ModelCommand extends Command
{
    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('make:model')
             ->addArgument('name', InputArgument::REQUIRED, 'The name for the model.')
             ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force to re-create model.')
             ->setDescription('Create a new model.')
             ->setHelp("This command makes you to create model...");
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Model name
        $name           = $input->getArgument('name');

        // Is Forced
        $force          = $input->getOption('force');

        // Class
        $class          = $name;

        // Namespace of the model
        $namespace      = 'App\\Models';

        // Location of the model
        $location       = ROOT . '/app/Models';

        // If the name is a path
        if (strpos($name, '/')) {
            $parts      = explode('/', $name);
            $class      = end($parts);
            array_pop($parts);
            $namespace  = 'App\\Models\\' . implode('\\', $parts);
            $location   = ROOT . '/app/Models/' . implode('/', $parts);
        }

        // Set file path
        $file = $location . '/' .  $class . '.php';

        if (!file_exists($file)) {
            $this->createNewFile($location, $class, $namespace);
            $output->writeln('<info>Success!</info> "' . ($name) . '" model created.');
        } else {
            if ($force !== false) {
                unlink($file);
                $this->createNewFile($location, $class, $namespace);
                $output->writeln('<info>Success!</info> "' . ($name) . '" model re-created.');
            } else {
                $output->writeln('<error>Error!</error> Model already exists! (' . $name . ')');
                return Command::FAILURE;
            }
        }

        return Command::SUCCESS;
    }

    /**
     * Create new model file
     *
     * @param string $location
     * @param string $class
     * @param string $namespace
     * @return void
     */
    private function createNewFile($location, $class, $namespace): void
    {
        // Create directories
        if (!is_dir($location)) {
            mkdir($location, 0755, true);
        }

        // Set path of class
        $file = $location . '/' . $class . '.php';

        $model = ucfirst($class);
        $contents = <<<PHP
<?php
namespace $namespace;

use DB;

class $model
{
    // Write your methods
}
PHP;
        if (false === file_put_contents($file, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $file));
        }
    }

}