<?php

namespace Titan\Console\Commands\Make;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ListenerCommand extends Command
{
    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('make:listener')
            ->addArgument('name', InputArgument::REQUIRED, 'The name for the listener.')
            ->addOption('event', 'e', InputOption::VALUE_REQUIRED, 'The name for the event to listen.')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force to re-create listener.')
            ->setDescription('Create new listener.')
            ->setHelp("This command makes you to create listener...");
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Listener name
        $name           = $input->getArgument('name');

        // Is Forced
        $force          = $input->getOption('force');

        // Event
        $event          = $input->getOption('event');

        // If event is not defined return an error
        if (empty($event)) {
            $output->writeln('<error>Error!</error> Please enter an event name.');
            return Command::FAILURE;
        }

        // Class
        $class          = $name;

        // Namespace of the listener
        $namespace      = 'App\\Listeners';

        // Location of the listener
        $location       = ROOT . '/app/Listeners';

        // If the name is a path
        if (strpos($name, '/')) {
            $output->writeln('<error>Error!</error> Listener must be in app/Listeners folder.');
            return Command::FAILURE;
        }

        // Set file path
        $file = $location . '/' .  $class . '.php';

        if (!file_exists($file)) {
            $this->createNewFile($location, $class, $namespace, $event);
            $output->writeln('<info>Success!</info> "' . ($name) . '" listener created.');
        } else {
            if ($force !== false) {
                unlink($file);
                $this->createNewFile($location, $class, $namespace, $event);
                $output->writeln('<info>Success!</info> "' . ($name) . '" listener re-created.');
            } else {
                $output->writeln('<error>Error!</error> Listener already exists! (' . $name . ')');
                return Command::FAILURE;
            }
        }

        return Command::SUCCESS;
    }

    /**
     * Create new listener file
     *
     * @param string $location
     * @param string $class
     * @param string $namespace
     * @param string $event
     * @return void
     */
    private function createNewFile($location, $class, $namespace, $event): void
    {
        // Create directories
        if (!is_dir($location)) {
            mkdir($location, 0755, true);
        }

        // Set path of class
        $file = $location . '/' . $class . '.php';

        $listener = ucfirst($class);
        $contents = <<<PHP
<?php

namespace $namespace;

use App\Events\\$event;

class $listener
{
    /**
     * Handle
     *
     * @param $event \$event
     */
    public function handle($event \$event)
    {
        // your code here...
    }
}
PHP;
        if (false === file_put_contents($file, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $file));
        }
    }
}