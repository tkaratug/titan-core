<?php

namespace Titan\Console\Commands\Make;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MigrationCommand extends Command
{
    protected string $table = 'table';

    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('make:migration')
             ->addArgument('name', InputArgument::REQUIRED, 'The name for the migration.')
             ->addOption('action', 'a', InputOption::VALUE_REQUIRED, 'What will this migration do?', 'create')
             ->addOption('table', 't', InputOption::VALUE_OPTIONAL, 'Table name.', 'table')
             ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force to re-create migration.')
             ->setDescription('Create a new migration.')
             ->setHelp('This command allows you to create new migration.');
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $class  = $input->getArgument('name');
        $action = $input->getOption('action');
        $force  = $input->getOption('force');

        $name   = date('Y_m_d_His') . '_' . $class;
        $file   = ROOT . '/database/migrations/' . $name . '.php';

        // Set the table name if it is given
        if ($input->hasOption('table')) {
            $this->table = $input->getOption('table');
        }

        if (!file_exists($file)) {
            $this->createNewFile($file, $class, $action);
            $output->writeln('<info>Success!</info> "' . ($name) . '" migration created.');
        } else {
            if ($force !== false) {
                unlink($file);
                $this->createNewFile($file, $class, $action);
                $output->writeln('<info>Success!</info> "' . ($name) . '" migration re-created.');
            } else {
                $output->writeln('<error>Error!</error> Migration already exists! (' . $name . ')');
                return Command::FAILURE;
            }
        }

        return Command::SUCCESS;
    }

    /**
     * Create new migration file
     *
     * @param string $file
     * @param string $name
     * @param string $action
     * @return void
     */
    private function createNewFile($file, $name, $action): void
    {
        if ($action == 'create') {
            $class  = 'CreateTable';
        } else if ($action == 'modify') {
            $class  = 'AlterTable';
        }

        $useSchema  = 'Opis\Database\Schema\\' . $class;
        $migration  = ucfirst($name);
        $contents   = <<<PHP
<?php
namespace Database\Migrations;

use Titan\Libraries\Database\Migration\Migration;
use $useSchema;

class $migration extends Migration
{
    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        \$this->db->schema()->$action('$this->table', function($class \$table) {
            // add columns
        });
    }
    
    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        \$this->db->schema()->drop('$this->table');
    }
}
PHP;
        if (false === file_put_contents($file, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $file));
        }
    }
}