<?php

namespace Titan\Console\Commands\Make;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SeederCommand extends Command
{
    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('make:seeder')
             ->addArgument('name', InputArgument::REQUIRED, 'The name for the seeder.')
             ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force to re-create seeder.')
             ->setDescription('Create a new seeder.')
             ->setHelp('This command allows you to create new seeder.');
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $class  = $input->getArgument('name');
        $force  = $input->getOption('force');

        $name   = date('Y_m_d_His') . '_' . $class;
        $file   = ROOT . '/database/seeders/' . $name . '.php';

        if (!file_exists($file)) {
            $this->createNewFile($file, $class);
            $output->writeln('<info>Success!</info> "' . ($name) . '" seeder created.');
        } else {
            if ($force !== false) {
                unlink($file);
                $this->createNewFile($file, $class);
                $output->writeln('<info>Success!</info> "' . ($name) . '" seeder re-created.');
            } else {
                $output->writeln('<error>Error!</error> Seeder already exists! (' . $name . ')');
                return Command::FAILURE;
            }
        }

        return Command::SUCCESS;
    }

    /**
     * Create new seeder file
     *
     * @param string $file
     * @param string $name
     * @return void
     */
    private function createNewFile($file, $name): void
    {
        $seeder     = ucfirst($name);
        $contents   = <<<PHP
<?php
namespace Database\Seeders;

use Titan\Libraries\Database\Seeder\Seeder;

class $seeder extends Seeder
{
    /**
     * Run the seeders
     *
     * @return void
     */
    public function up()
    {
        // add columns..
    }
    
    /**
     * Reverse the seeders
     *
     * @return void
     */
    public function down()
    {
        // your codes here...
    }
}
PHP;
        if (false === file_put_contents($file, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $file));
        }
    }
}