<?php

namespace Titan\Console\Commands\Make;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class EventCommand extends Command
{
    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('make:event')
             ->addArgument('name', InputArgument::REQUIRED, 'The name for the event.')
             ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force to re-create event.')
             ->setDescription('Create new event.')
             ->setHelp("This command makes you to create event...");
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Event name
        $name           = $input->getArgument('name');

        // Is Forced
        $force          = $input->getOption('force');

        // Class
        $class          = $name;

        // Namespace of the event
        $namespace      = 'App\\Events';

        // Location of the event
        $location       = ROOT . '/app/Events';

        // If the name is a path
        if (strpos($name, '/')) {
            $output->writeln('<error>Error!</error> Event must be in app/Events folder.');
            return Command::FAILURE;
        }

        // Set file path
        $file = $location . '/' .  $class . '.php';

        if (!file_exists($file)) {
            $this->createNewFile($location, $class, $namespace);
            $output->writeln('<info>Success!</info> "' . ($name) . '" event created.');
        } else {
            if ($force !== false) {
                unlink($file);
                $this->createNewFile($location, $class, $namespace);
                $output->writeln('<info>Success!</info> "' . ($name) . '" event re-created.');
            } else {
                $output->writeln('<error>Error!</error> Event already exists! (' . $name . ')');
                return Command::FAILURE;
            }
        }

        return Command::SUCCESS;
    }

    /**
     * Create new event file
     *
     * @param string $location
     * @param string $class
     * @param string $namespace
     * @return void
     */
    private function createNewFile($location, $class, $namespace): void
    {
        // Create directories
        if (!is_dir($location)) {
            mkdir($location, 0755, true);
        }

        // Set path of class
        $file = $location . '/' . $class . '.php';

        $event = ucfirst($class);
        $contents = <<<PHP
<?php

namespace $namespace;

class $event
{
    /**
     * Event constructor.
     */
    public function __construct()
    {
        // your code here...
    }
}
PHP;
        if (false === file_put_contents($file, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $file));
        }
    }
}