<?php

namespace Titan\Console\Commands\Make;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MiddlewareCommand extends Command
{
    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('make:middleware')
             ->addArgument('name', InputArgument::REQUIRED, 'The name for the middleware.')
             ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force to re-create middleware.')
             ->setDescription('Create a new middleware.')
             ->setHelp("This command makes you to create middleware...");
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Middleware name
        $name           = $input->getArgument('name');

        // Is Forced
        $force          = $input->getOption('force');

        // Class
        $class          = $name;

        // Namespace of the middleware
        $namespace      = 'App\\Middlewares';

        // Location of the middleware
        $location       = ROOT . '/app/Middlewares';

        // If the name is a path
        if (strpos($name, '/')) {
            $parts      = explode('/', $name);
            $class      = end($parts);
            array_pop($parts);
            $namespace  = 'App\\Middlewares\\' . implode('\\', $parts);
            $location   = ROOT . '/app/Middlewares/' . implode('/', $parts);
        }

        // Set file path
        $file = $location . '/' .  $class . '.php';

        if (!file_exists($file)) {
            $this->createNewFile($location, $class, $namespace);
            $output->writeln('<info>Success!</info> "' . ($name) . '" middleware created.');
        } else {
            if ($force !== false) {
                unlink($file);
                $this->createNewFile($location, $class, $namespace);
                $output->writeln('<info>Success!</info> "' . ($name) . '" middleware re-created.');
            } else {
                $output->writeln('<error>Error!</error> Middleware already exists! (' . $name . ')');
                return Command::FAILURE;
            }
        }

        return Command::SUCCESS;
    }

    /**
     * Create new middleware file
     *
     * @param string $location
     * @param string $class
     * @param string $namespace
     * @return void
     */
    private function createNewFile($location, $class, $namespace): void
    {
        // Create directories
        if (!is_dir($location)) {
            mkdir($location, 0755, true);
        }

        // Set path of class
        $file = $location . '/' . $class . '.php';

        $middleware = ucfirst($class);
        $contents = <<<PHP
<?php
namespace $namespace;

class $middleware
{
    /**
     * Run before the action
     */
    public function handle()
    {
        // your code here...
    }
}
PHP;
        if (false === file_put_contents($file, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $file));
        }
    }

}