<?php

namespace Titan\Console\Commands\Make;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ControllerCommand extends Command
{
    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('make:controller')
             ->addArgument('name', InputArgument::REQUIRED, 'The name for the controller.')
             ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force to re-create controller.')
             ->setDescription('Create a new controller.')
             ->setHelp("This command makes you to create controller...");
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Controller name
        $name           = $input->getArgument('name');

        // Is Forced
        $force          = $input->getOption('force');

        // Class
        $class          = $name;

        // Namespace of the controller
        $namespace      = 'App\\Controllers';

        // Location of the controller
        $location       = ROOT . '/app/Controllers';

        // If the name is a path
        if (strpos($name, '/')) {
            $parts      = explode('/', $name);
            $class      = end($parts);
            array_pop($parts);
            $namespace  = 'App\\Controllers\\' . implode('\\', $parts);
            $location   = ROOT . '/app/Controllers/' . implode('/', $parts);
        }

        // Set file path
        $file = $location . '/' .  $class . '.php';

        if (!file_exists($file)) {
            $this->createNewFile($location, $class, $namespace);
            $output->writeln('<info>Success!</info> "' . ($name) . '" controller created.');
        } else {
            if ($force !== false) {
                unlink($file);
                $this->createNewFile($location, $class, $namespace);
                $output->writeln('<info>Success!</info> "' . ($name) . '" controller re-created.');
            } else {
                $output->writeln('<error>Error!</error> Controller already exists! (' . $name . ')');
                return Command::FAILURE;
            }
        }

        return Command::SUCCESS;
    }

    /**
     * Create new controller file
     *
     * @param string $location
     * @param string $class
     * @param string $namespace
     * @return void
     */
    private function createNewFile($location, $class, $namespace): void
    {
        // Create directories
        if (!is_dir($location)) {
            mkdir($location, 0755, true);
        }

        // Set path of class
        $file = $location . '/' . $class . '.php';

        $controller = ucfirst($class);
        $contents = <<<PHP
<?php

namespace $namespace;

use Titan\Controller\Controller;
use Titan\Libraries\Http\Request\Request;

class $controller extends Controller
{
    /**
     * @param Request \$request
     */
    public function index(Request \$request)
    {
        // your code here...
    }
}
PHP;
        if (false === file_put_contents($file, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $file));
        }
    }

}