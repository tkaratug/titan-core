<?php

namespace Titan\Console\Commands\Make;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ServiceCommand extends Command
{
    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('make:service')
            ->addArgument('name', InputArgument::REQUIRED, 'The name for the service.')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Force to re-create service.')
            ->setDescription('Create a new service.')
            ->setHelp("This command makes you to create service...");
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Service name
        $name           = $input->getArgument('name');

        // Is Forced
        $force          = $input->getOption('force');

        // Class
        $class          = $name;

        // Namespace of the service
        $namespace      = 'App\\Services';

        // Location of the service
        $location       = ROOT . '/app/Services';

        // If the name is a path
        if (strpos($name, '/')) {
            $parts      = explode('/', $name);
            $class      = end($parts);
            array_pop($parts);
            $namespace  = 'App\\Services\\' . implode('\\', $parts);
            $location   = ROOT . '/app/Services/' . implode('/', $parts);
        }

        // Set file path
        $file = $location . '/' .  $class . '.php';

        if (!file_exists($file)) {
            $this->createNewFile($location, $class, $namespace);
            $output->writeln('<info>Success!</info> "' . ($name) . '" service created.');
        } else {
            if ($force !== false) {
                unlink($file);
                $this->createNewFile($location, $class, $namespace);
                $output->writeln('<info>Success!</info> "' . ($name) . '" service re-created.');
            } else {
                $output->writeln('<error>Error!</error> Service already exists! (' . $name . ')');
                return Command::FAILURE;
            }
        }

        return Command::SUCCESS;
    }

    /**
     * Create new service file
     *
     * @param string $location
     * @param string $class
     * @param string $namespace
     * @return void
     */
    private function createNewFile($location, $class, $namespace): void
    {
        // Create directories
        if (!is_dir($location)) {
            mkdir($location, 0755, true);
        }

        // Set path of class
        $file = $location . '/' . $class . '.php';

        $service    = ucfirst($class);
        $contents   = <<<PHP
<?php

namespace $namespace;

class $service
{
    /**
     * Method
     */
    public function foo()
    {
        // your code here...
    }
}
PHP;
        if (false === file_put_contents($file, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $file));
        }
    }

}