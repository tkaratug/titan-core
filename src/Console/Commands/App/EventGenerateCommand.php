<?php

namespace Titan\Console\Commands\App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Titan\Kernel\Application;

class EventGenerateCommand extends Command
{
    protected Application $app;

    /**
     * EventGenerateCommand constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct();
        $this->app = $app;
    }

    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('events:generate')
             ->setDescription('Generate events and listeners from service config file.')
             ->setHelp("This command makes you to create events and listeners that have been set in the service config file...");
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Get events and listeners from service config file
        $events = $this->app->resolve('config')->get('services.events.app');

        if (empty($events)) {
            $output->writeln('<error>Warning!</error> Couldn\'t find any events to generate.');
            return Command::FAILURE;
        }

        $eventOutput    = [];
        $listenerOutput = [];

        foreach ($events as $event => $listeners) {
            $eventPath = str_replace('\\', '/', $event);
            $eventPart = explode('/', $eventPath);
            $eventFile = end($eventPart);
            array_pop($eventPart);
            $eventPart[0] = 'app';
            $eventPath = implode('/', $eventPart);

            if (!file_exists($eventPath . '/' . $eventFile . '.php')) {
                // Create event file
                $this->createEvent($eventPath, $eventFile);

                // Add event to output
                $eventOutput[] = $event;

                foreach ($listeners as $listener) {
                    $listenerPath = str_replace('\\', '/', $listener);
                    $listenerPart = explode('/', $listenerPath);
                    $listenerFile = end($listenerPart);
                    array_pop($listenerPart);
                    $listenerPart[0] = 'app';
                    $listenerPath = implode('/', $listenerPart);

                    if (!file_exists($listenerPath . '/' . $listenerFile . '.php')) {
                        // Create listener file
                        $this->createListener($listenerPath, $listenerFile, $event);

                        $listenerOutput[$event][] = $listener;
                    } else {
                        $output->writeln('<info>Error!</info> [' . $listener . '] - Listener already exists.');
                    }
                }
            } else {
                $output->writeln('<info>Error!</info> [' . $event . '] - Event already exists.');
            }
        }

        // Set outputs
        foreach ($eventOutput as $event) {
            $output->writeln('<info>Success!</info> [' . $event . '] - Event file successfully created.');

            foreach ($listenerOutput[$event] as $listener) {
                $output->writeln('<info>Success!</info> ['. $listener .'] - Listener file successfully created.');
            }
        }

        return Command::SUCCESS;
    }

    /**
     * Create event file
     *
     * @param string $eventPath
     * @param string $eventFile
     */
    private function createEvent(string $eventPath, string $eventFile): void
    {
        // Create directories
        if (!is_dir($eventPath)) {
            mkdir($eventPath, 0755, true);
        }

        // Set path of class
        $file = $eventPath . '/' . $eventFile . '.php';

        $event = ucfirst($eventFile);
        $contents = <<<PHP
<?php

namespace App\Events;

class $event
{
    /**
     * Event constructor.
     */
    public function __construct()
    {
        // your code here...
    }
}
PHP;
        if (false === file_put_contents($file, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $file));
        }

    }

    /**
     * Create listener file
     *
     * @param string $listenerPath
     * @param string $listenerFile
     * @param string $event
     */
    private function createListener(string $listenerPath, string $listenerFile, string $event): void
    {
        // Create directories
        if (!is_dir($listenerPath)) {
            mkdir($listenerPath, 0755, true);
        }

        // Set path of class
        $file = $listenerPath . '/' . $listenerFile . '.php';

        $eventPart = explode('\\', $event);
        $eventName = end($eventPart);

        $listener = ucfirst($listenerFile);
        $contents = <<<PHP
<?php

namespace App\Listeners;

use $event;

class $listener
{
    /**
     * Handle
     *
     * @param $eventName \$event
     */
    public function handle($eventName \$event)
    {
        // your code here...
    }
}
PHP;
        if (false === file_put_contents($file, $contents)) {
            throw new \RuntimeException(sprintf('The file "%s" could not be written to', $file));
        }
    }
}