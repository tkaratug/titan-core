<?php

namespace Titan\Console\Commands\App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Titan\Kernel\Application;

class KeyCommand extends Command
{
    protected Application $app;

    /**
     * KeyCommand constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct();
        $this->app = $app;
    }

    protected function configure()
    {
        $this->setName('app:key')
            ->setDescription('Generate and set the application key.')
            ->setHelp('This command makes you to generate and set the application key.')
            ->addOption('show', 's', InputOption::VALUE_NONE, 'Show the key instead of modifying .env file');
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $key = $this->generateKey();

        if ($input->getOption('show') !== false) {
            $output->writeln('<comment>' . $key . '</comment>');
            return Command::SUCCESS;
        }

        if (!$this->setKeyInEnvironmentFile($key, $input, $output)) {
            return Command::SUCCESS;
        }

        $output->writeln('<info>Success</info> Application key set successfully');

        return Command::SUCCESS;
    }

    /**
     * Generate ransom string
     *
     * @param int $length
     * @param bool $strong
     * @return string
     */
    private function generateKey(int $length = 32, bool $strong = true): string
    {
        if (function_exists('openssl_random_pseudo_bytes')) {
            $token = base64_encode(openssl_random_pseudo_bytes($length, $strong));
            return strtr(substr($token, 0, $length), '+/=', '-_,');
        }

        $characters = '0123456789';
        $characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz/+';
        $charactersLength = strlen($characters)-1;
        $token = '';

        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[mt_rand(0, $charactersLength)];
        }

        return $token;
    }

    /**
     * @param string $key
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool
     */
    private function setKeyInEnvironmentFile(string $key, InputInterface $input, OutputInterface $output): bool
    {
        // Get the current key
        $currentKey = $this->app->resolve('config')->get('app.key');

        // Set helper and ask the question to move
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Application key will re-generate. Are you sure?: ', false);
        if (strlen($currentKey) !== 0 && (!$helper->ask($input, $output, $question))) {
            return false;
        }

        // Write the new generated key to the .env file
        $this->writeNewKeyToEnvFile($key);

        // return true
        return true;
    }

    /**
     * Write the new generated key to the .env file
     *
     * @param string $key
     */
    private function writeNewKeyToEnvFile(string $key): void
    {
        $basePath = $this->app->get('root_path');

        file_put_contents($basePath . '/.env', preg_replace(
            $this->keyReplacementPattern(), 'APP_KEY=' . $key, file_get_contents($basePath . '/.env')
        ));
    }

    /**
     * @return string
     */
    private function keyReplacementPattern(): string
    {
        $escaped = preg_quote('=' . $this->app->resolve('config')->get('app.key'), '/');
        return "/^APP_KEY{$escaped}/m";
    }
}