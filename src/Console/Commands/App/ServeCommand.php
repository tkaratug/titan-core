<?php

namespace Titan\Console\Commands\App;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ServeCommand extends Command
{
    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('serve')
            ->setDescription('Start application on your local server (Default: 127.0.0.1:8000).')
            ->setHelp('This command allows you to serve the application on your local server.')
            ->addOption('port', 'p', InputOption::VALUE_OPTIONAL, 'The port to serve the application on (Default: 8000)')
            ->addOption('host', 's', InputOption::VALUE_OPTIONAL, 'The host address to serve the application on (Default: 127.0.0.1)');
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $host = $input->getOption('host');
        $host = (!empty($host) ? $host : '127.0.0.1');

        $port = $input->getOption('port');
        $port = (!empty($port) ? $port : '8000');

        $output->writeln("<info>Titan is running on (http://{$host}:{$port}/)" . "\n" . "Press Ctrl+C to quit</info>" . "\n");
        passthru('php -S ' . $host . ':' . $port . ' -t public');

        return Command::SUCCESS;
    }
}
