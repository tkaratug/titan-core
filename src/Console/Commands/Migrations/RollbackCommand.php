<?php

namespace Titan\Console\Commands\Migrations;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Titan\Kernel\Application;

class RollbackCommand extends Command
{
    protected Application $app;

    /**
     * RollbackCommand constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct();
        $this->app = $app;
    }

    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('db:rollback')
             ->setDescription('Rollback the last batch of migrations.')
             ->setHelp("This command makes you to roll back the last batch of migrations...");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->app->resolve('migration')->rollback($output);
        return Command::SUCCESS;
    }
}
