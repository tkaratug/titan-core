<?php

namespace Titan\Console\Commands\Migrations;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Titan\Kernel\Application;

class MigrateCommand extends Command
{
    protected Application $app;

    /**
     * MigrateCommand constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        parent::__construct();
        $this->app = $app;
    }

    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('db:migrate')
             ->setDescription('Run all migrations.')
             ->setHelp("This command makes you to run all migrations...")
             ->addOption('source', 's', InputOption::VALUE_REQUIRED, 'Specific migration name', 'all')
             ->addOption('fresh', 'f', InputOption::VALUE_OPTIONAL, 'Delete all tables and migrate again', false);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->app->resolve('migration')->run($input, $output);
        return Command::SUCCESS;
    }
}