<?php

namespace Titan\Middleware;

use Exception;
use Titan\Libraries\Http\Request\Request;

class VerifyCsrf
{
    /**
     * Verfiy CSRF token
     *
     * @param Request $request
     * @param array $currentRoute
     * @param array $except
     * @return bool
     * @throws Exception
     */
    protected function verify(Request $request, array $currentRoute, array $except = []): bool
    {
        if (!in_array($currentRoute['name'], $except) && $request->isMethod('post')) {

            if ($request->isAjax()) {
                $token = $request->header('X-CSRF-TOKEN');

                if (empty($token)) {
                    throw new Exception('Missing CSRF header token.');
                }
            } else {
                $token = $request->post('csrf_token');

                if (empty($token)) {
                    throw new Exception('Missing CSRF form token.');
                }
            }

            if (!csrf_check($token)) {
                throw new Exception('CSRF token mismatch.');
            }
        }

        return true;
    }
}