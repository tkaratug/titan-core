<?php

namespace Titan\Kernel;

abstract class ServiceProvider
{
    protected Application $app;

    /**
     * ServiceProvider constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }
}