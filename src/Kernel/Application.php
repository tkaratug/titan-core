<?php

namespace Titan\Kernel;

use Titan\Container;
use Titan\Libraries\Http\Request\Request;
use Tracy\Debugger;

class Application extends Container
{
    /**
     * Environment
     */
    private string $environment;

    /**
     * Root directory
     */
    private string $rootPath;

    /**
     * Base directory
     */
    private string $basePath;

    /**
     * App directory
     */
    private string $appPath;

    /**
     * Configuration parameters
     */
    private array $config = [];

    /**
     * Kernel constructor.
     * @param string $environment
     */
    public function __construct(string $environment)
    {
        $this->environment  = $environment;
        $this->rootPath     = str_replace('public', '', realpath(getcwd()));
        $this->basePath     = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1));
        $this->appPath      = $this->rootPath . '/app';

        $this->register();
    }

    /**
     * Register paths, service providers and facades
     */
    protected function register(): void
    {
        $this->loadConfigFiles();
        $this->registerPaths();
        $this->registerPrimaryServiceProviders();
        $this->registerServiceProviders($this->resolve('config')->get('services.providers'));
        $this->registerMiddleware($this->resolve('config')->get('services.middleware'));
        $this->registerServices();
        $this->registerCommands($this->resolve('config')->get('commands'));
        $this->resolveFacades($this->resolve('config')->get('services.facades'));
        $this->bootServiceProviders($this->resolve('config')->get('services.providers'));
    }

    /**
     * Kernel run
     *
     * @param Request $request
     * @return void
     */
    public function run(Request $request): void
    {
        $this->setEnvironment($this->environment);
        $this->setLanguage();

        $this->resolve('load')->file(realpath($this->rootPath . '/app/routes.php'));
        $this->resolve('router', [$request])->run();
    }

    /**
     * Set environment
     *
     * @param string $env
     * @return void
     */
    protected function setEnvironment(string $env): void
    {
        switch ($env) {
            case 'dev':
                ini_set('display_errors', 1);
                error_reporting(1);
                Debugger::enable();
                break;
            case 'test':
            case 'prod':
                ini_set('display_errors', 0);
                error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
                break;
            default:
                header('HTTP/1.1 503 Service Unavailable.', true, 503);
                die('The application environment is not set correctly.');
        }
    }

    /**
     * Set default language
     */
    protected function setLanguage(): void
    {
        $config     = $this->resolve('config')->get('app');
        $session    = $this->resolve('session');

        if (!$session->has('lang')) {
            $session->set('lang', $config['languages'][$config['locale']]);
        }
    }

    /**
     * Load all config files
     *
     * @return void
     */
    protected function loadConfigFiles(): void
    {
        // Start dotenv
        $repository = \Dotenv\Repository\RepositoryBuilder::createWithNoAdapters()
            ->addAdapter(\Dotenv\Repository\Adapter\EnvConstAdapter::class)
            ->addWriter(\Dotenv\Repository\Adapter\PutenvAdapter::class)
            ->immutable()
            ->make();

        $dotenv = \Dotenv\Dotenv::create($repository, $this->rootPath);
        $dotenv->load();

        // Get all config parameters
        foreach (glob($this->rootPath . '/config/*.php') as $file) {
            $key = strtolower(str_replace([$this->rootPath . '/config/', '.php'], '', $file));
            $this->config[$key] = require $file;
        }

        // Store all config parameters in container
        $this->store('config_params', $this->config);
    }

    /**
     * Register primary providers to the container
     *
     * @return void
     */
    protected function registerPrimaryServiceProviders(): void
    {
        (new \Titan\Libraries\Load\LoadServiceProvider($this))->register();
        (new \Titan\Libraries\Config\ConfigServiceProvider($this))->register();
    }

    /**
     * Register service providers of Application
     *
     * @param array $providers
     * @return void
     */
    protected function registerServiceProviders(array $providers): void
    {
        foreach ($providers as $provider) {
            if (method_exists($provider, 'register')) {
                (new $provider($this))->register();
            }
        }
    }

    /**
     * Register middleware
     *
     * @param array $middleware
     * @return void
     */
    protected function registerMiddleware(array $middleware): void
    {
        foreach ($middleware as $loadKey => $loadVal) {
            foreach ($loadVal as $key => $val) {
                $this->bind($val)->alias($key);
            }
        }
    }

    /**
     * Register services
     *
     * @return void
     */
    protected function registerServices(): void
    {
        $services = $this->getDirContents($this->appPath . DIRECTORY_SEPARATOR . 'Services', '/\Service.php$/');

        foreach ($services as $service) {
            $service = ucfirst(substr($service, strpos($service, 'app' . DIRECTORY_SEPARATOR)));
            $serviceParts = explode(DIRECTORY_SEPARATOR, $service);
            $serviceName = str_replace('.php', '', end($serviceParts));

            array_pop($serviceParts);

            $serviceParts = array_map(function ($item) {
                return ucfirst($item);
            }, $serviceParts);

            $this->bind(implode('\\', $serviceParts) . '\\' . $serviceName)->alias($serviceName);
        }
    }

    /**
     * Register commands
     *
     * @param array $commands
     * @return void
     */
    protected function registerCommands(array $commands): void
    {
        foreach ($commands as $key => $value) {
            $this->bind($value)->alias($key);
        }
    }

    /**
     * Resolve facades
     *
     * @param array $facades
     * @return void
     */
    protected function resolveFacades(array $facades): void
    {
        Facade::setFacadeApplication($this);

        foreach ($facades as $key => $val) {
            if (!class_exists($key)) {
                class_alias($val, $key);
            }
        }
    }

    /**
     * Bootstrap application services
     *
     * @param array $providers
     */
    protected function bootServiceProviders(array $providers): void
    {
        foreach ($providers as $provider) {
            if (method_exists($provider, 'boot')) {
                (new $provider($this))->boot();
            }
        }
    }

    /**
     * Register paths to the container
     */
    protected function registerPaths(): void
    {
        $this->store('root_path', $this->rootPath);
        $this->store('base_path', $this->basePath);
        $this->store('app_path', $this->rootPath . '/app');
        $this->store('model_path', $this->appPath . '/Models');
        $this->store('view_path', $this->appPath . '/Views');
        $this->store('config_path', $this->rootPath . '/config');
        $this->store('asset_path', $this->rootPath . '/public/assets');
        $this->store('storage_path', $this->rootPath . '/storage');
    }

    /**
     * Get nested directories and files.
     *
     * @param string $directory
     * @param string $filter
     * @param array $results
     * @return array
     */
    private function getDirContents(string $directory, string $filter = '', array &$results = []): array
    {
        $files = scandir($directory);

        foreach ($files as $key => $value) {
            $path = realpath($directory . DIRECTORY_SEPARATOR . $value);

            if (!is_dir($path)) {
                if (empty($filter) || preg_match($filter, $path)) {
                    $results[] = $path;
                }
            } elseif ($value !== '.' && $value !== '..') {
                $this->getDirContents($path, $filter, $results);
            }
        }

        return $results;
    }
}