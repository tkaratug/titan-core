<?php

if (!function_exists('app')) {
    /**
     * Get application container or a service
     *
     * @param string|null $name
     * @return mixed
     */
    function app($name = null)
    {
        if (is_null($name))
            return \Titan\Kernel\Facade::getFacadeApplication();

        return app()->resolve($name);
    }
}

if (!function_exists('asset')) {
    /**
     * Return the path of asset
     *
     * @param string $file
     * @return string
     */
    function asset(string $file) : string
    {
        return app()->get('base_path') . '/assets/' . $file;
    }
}

if (!function_exists('env')) {
    /**
     * Return the value of an environment variable
     *
     * @param $key
     * @param null $default
     * @return array|bool|false|string|void|null
     */
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false || $value === '') {
            return $default;
        }

        switch (strtolower($value)) {
            case 'true':
                return true;

            case 'false':
                return false;

            case 'null':
                return;
        }

        return $value;
    }
}

if (!function_exists('root_path')) {
    /**
     * Return root directory
     *
     * @param string|null $path
     * @return string
     */
    function root_path($path = null): string
    {
        $rootPath = app()->get('root_path');

        return is_null($path) ? $rootPath : $rootPath . DS . $path;
    }
}

if (!function_exists('base_path')) {
    /**
     * Return base directory
     *
     * @param string|null $path
     * @return string
     */
    function base_path($path = null): string
    {
        $basePath = app()->get('base_path');

        return is_null($path) ? $basePath : $basePath . DS . $path;
    }
}

if (!function_exists('app_path')) {
    /**
     * Return app directory
     *
     * @param string|null $path
     * @return string
     */
    function app_path($path = null): string
    {
        $appPath = app()->get('app_path');

        return is_null($path) ? $appPath : $appPath . DS . $path;
    }
}

if (!function_exists('asset_path')) {
    /**
     * Return assets directory
     *
     * @param string|null $path
     * @return string
     */
    function asset_path($path = null): string
    {
        $assetPath = app()->get('asset_path');

        return is_null($path) ? $assetPath : $assetPath . DS . $path;
    }
}

if (!function_exists('model_path')) {
    /**
     * Return models directory
     *
     * @param string|null $path
     * @return string
     */
    function model_path($path = null): string
    {
        $modelPath = app()->get('model_path');

        return is_null($path) ? $modelPath : $modelPath . DS . $path;
    }
}

if (!function_exists('view_path')) {
    /**
     * Return views directory
     *
     * @param string|null $path
     * @return string
     */
    function view_path($path = null): string
    {
        $viewPath = app()->get('view_path');

        return is_null($path) ? $viewPath : $viewPath . DS . $path;
    }
}

if (!function_exists('config_path')) {
    /**
     * Return config directory
     *
     * @param string|null $path
     * @return string
     */
    function config_path($path = null): string
    {
        $configPath = app()->get('config_path');

        return is_null($path) ? $configPath : $configPath . DS . $path;
    }
}

if (!function_exists('storage_path')) {
    /**
     * Return storage directory
     *
     * @param string|null $path
     * @return string
     */
    function storage_path($path = null): string
    {
        $storagePath = app()->get('storage_path');

        return is_null($path) ? $storagePath : $storagePath . DS . $path;
    }
}

if (!function_exists('link_to')) {
    /**
     * Return url
     *
     * @param $url
     * @return string
     */
    function link_to($url): string
    {
        return app()->get('base_path') . '/' . $url;
    }
}

if (!function_exists('auth')) {
    /**
     * Get authenticated user
     *
     * @return mixed
     */
    function auth()
    {
        return app()->resolve('auth');
    }
}

if (!function_exists('config')) {
    /**
     * Get config service or an item
     *
     * @param string|null $item
     * @return mixed
     */
    function config($item = null)
    {
        return \Titan\Facades\Config::get($item);
    }
}

if (!function_exists('event')) {
    /**
     * Fire each listener that belongs to the event
     *
     * @param string $event
     * @return mixed
     */
    function event($event) {
        return \Titan\Facades\Event::fire($event);
    }
}

if (!function_exists('view')) {
    /**
     * View
     *
     * @return mixed
     */
    function view() {
        return app()->resolve('view');
    }
}

if (!function_exists('translate')) {
    /**
     * Translate
     *
     * @param $file
     * @param $key
     * @param null $change
     * @return mixed
     */
    function translate($file, $key, $change = null)
    {
        return \Titan\Facades\Language::translate($file, $key, $change);
    }
}

if (!function_exists('route')) {
    /**
     * Get url from route
     *
     * @param $name
     * @param array $params
     * @return ?string
     */
    function route($name, $params = []): ?string
    {
        return \Titan\Facades\Router::getUrl($name, $params);
    }
}

if (!function_exists('redirect')) {
    /**
     * Redirect
     *
     * @param $url
     * @param int $delay
     * @return mixed
     */
    function redirect($url, $delay = 0)
    {
        return \Titan\Facades\Uri::redirect($url, $delay);
    }
}

if (!function_exists('csrf_token')) {
    /**
     * Define csrf token
     *
     * @return string
     */
    function csrf_token(): string
    {
        // If there are any tokens in session, get them
        if (\Titan\Facades\Session::has('csrf_token')) {
            return \Titan\Facades\Session::get('csrf_token');
        }

        // Generate token
        $token = base64_encode(openssl_random_pseudo_bytes(32));

        // Store token list in session
        \Titan\Facades\Session::set('csrf_token', $token);

        // Return token
        return $token;
    }
}

if (!function_exists('csrf_check')) {
    /**
     * Validate csrf token
     *
     * @param string $token
     * @return boolean
     */
    function csrf_check($token): bool
    {
        if (\Titan\Facades\Session::has('csrf_token')) {

            // If the given token in csrf_tokens list
            if ($token == \Titan\Facades\Session::get('csrf_token')) {
                // Remove csrf_tokens from the session
                \Titan\Facades\Session::delete('csrf_token');

                // Return true
                return true;
            }
        }

        return false;
    }
}