<?php

namespace Titan\Exception\Contracts;

interface HttpExceptionInterface
{
    /**
     * The HTTP status code
     *
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * Return headers
     *
     * @return array
     */
    public function getHttpHeaders(): array;

    /**
     * Data corresponding to the error
     *
     * @return array
     */
    public function getErrorData(): array;

    /**
     * String code of the error
     *
     * i.e. not_found or access_denied
     *
     * @return string
     */
    public function getError(): string;

    /**
     * A user-friendly error description
     *
     * @return string
     */
    public function getUserMessage(): string;
}