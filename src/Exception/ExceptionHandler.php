<?php

namespace Titan\Exception;

use Exception;

class ExceptionHandler extends Exception
{
    public function __construct($title, $message)
    {
        if (APP_ENV !== 'prod') {
            throw new Exception(strip_tags($title . ' - ' . $message), 1);
        }

        return require __DIR__ . '/Views/error.php';
    }
}